# The CN2 induction algorithm
## A rule-based classifier
Supervised and Experiential Learning (SEL) - Master in Artificial Intelligence

## Author
- Armando Rodriguez Ramos

## Running script for the first time
These sections show how to create virtual environment for
our script and how to install dependencies. The first thing to do is to install Python 3.9.
1. Open folder in terminal
```bash
cd <root_folder_of_project>/
```
2. Create virtual env
```bash
python3 -m venv venv/
```
3. Open virtual env
```bash
source venv/bin/activate
```
4. Install required dependencies
```bash
pip install -r requirements.txt
```
## Execute code
1. Open Source folder in terminal
```bash
cd Source/
```
2. Running the main code

- You can modify the parameters on the main function. The steps to use it will be:
  - Instantiate the CN2 object with the requeried K and Min_significance parameters.
  - Choose any dataset in CSV format.
  - Correctly set all the necessary parameters of the fit method in order to correctly read the CSV (separation, header, classCol, etc.). Also remember to indicate the numerical or binary columns with numCols and binCols respectively.
  - Vary the verbose parameter on the first step to see the process of the rules as well as some details on their quality.

- By default, the main function will execute all the datasets of the report with the maximum level of verbose.
 ```bash
 python3 main.py
 ```
3. Close virtual environment
```bash
deactivate
```
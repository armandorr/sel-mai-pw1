import time
import numpy as np
import pandas as pd
from classes.CN2 import CN2


def crossValidationExperiment(path, alg_k, alg_sig, numCols=[], cross_k=5, colNames=None):
    """
    Function to compute cross validation experiment with a given dataset path and needed k and significance parameters

    :param path: The path were the CSV is located.
    :param alg_k: The k value that indicates the amount of complexes to maintain in each iteration in CN2.
    :param alg_sig: The min significance to consider while performing the specialization process in CN2.
    :param numCols: The numerical columns of the CSV that have to be discretized.
    :param cross_k: The cross validation k (i.e. the amount of experiments to perform).
    :param colNames: The names of the columns to be used. If none [col_0, col_1, ...] will be used.
    """
    name = path.split('/')[-1][:-4]
    np.random.seed(42)
    seeds = np.random.randint(0, 100000, cross_k)
    partitions = []
    rules = []
    for idx, seed in enumerate(seeds):
        print(f"\nSeed nº{idx} = {seed}")
        starTime = time.time()
        cn2 = CN2(k=alg_k, min_significance=alg_sig, seed=seed, verbose=0)
        trainX, testX, trainY, testY = cn2.prepareData(path, numCols=numCols, colNames=colNames)
        partitions.append((trainX, testX, trainY, testY))
        cn2.fit(path, numCols=numCols, colNames=colNames)
        rules.append(cn2.get_rules())
        endTime = time.time()
        timeDiff = endTime - starTime
        print(f"Computed in {round(timeDiff // 60, 2)} minutes and {round(timeDiff % 60, 2)} seconds")

    print("Obtain all partitions")
    cn2 = CN2(k=alg_k, min_significance=alg_sig, verbose=0)
    for idx, (rule, (X, Xt, Y, Yt)) in enumerate(zip(rules, partitions)):
        accuracy, precisions, recalls = cn2.score(X, Y, rules=rule, train=True)
        accuracyTest, _, _ = cn2.score(Xt, Yt, rules=rule, train=False)
        results = np.zeros((len(rule), 5)).astype(object)
        for jdx, (precision, recall) in enumerate(zip(precisions, recalls)):
            results[jdx, 0] = str(rule[jdx])
            results[jdx, 1] = round(precision * 100, 2)
            results[jdx, 2] = round(recall * 100, 2)
            results[jdx, 3] = round(accuracy, 2)
            results[jdx, 4] = round(accuracyTest, 2)

        pd.DataFrame(results.reshape(-1, 5), columns=["Rule", "Precision", "Recall", "AccuracyTrain", "AccuracyTest"]).\
            to_csv(f"../Results/results_{name}/partition{idx+1}Cross.csv", index=False)

    print("Compute precision/coverage with train")
    num = 0
    cn2 = CN2(k=alg_k, min_significance=alg_sig, verbose=0)
    results = np.zeros((sum([len(rule) for rule in rules]), cross_k, 5)).astype(object)
    for jdx, rule in enumerate(rules):
        print(f"----------------------------- SET OF RULES {jdx} -----------------------------")
        for idx, (X, _, Y, _) in enumerate(partitions):
            accuracy, precisions, recalls = cn2.score(X, Y, rules=rule, train=True)
            numIn = 0
            for precision, recall in zip(precisions, recalls):
                results[num + numIn, idx, 0] = str(rule[numIn])
                results[num + numIn, idx, 1] = idx
                results[num + numIn, idx, 2] = jdx
                results[num + numIn, idx, 3] = round(precision * 100, 2)
                results[num + numIn, idx, 4] = round(recall * 100, 2)
                numIn += 1

        num += len(rule)

    pd.DataFrame(results.reshape(-1, 5), columns=["Rule", "Partition", "RuleSet", "Precision", "Recall"]). \
        to_csv(f"../Results/results_{name}/results{name}Cross.csv", index=False)


def allRulesLatex(name, cross_k=5):
    """
    Function that creates the set of rules to be added to latex tables. Notice that the function
    crossValidationExperiment has to be called before in order to have the needed documents.

    :param name: The name of the dataset.
    :param cross_k: The cross validation k (i.e. the amount of experiments that have been performed).
    """
    accsTrain = []
    accsTest = []
    with open(f"../Results/results_{name}/allLatex.txt", 'w', encoding="utf-8") as f:
        for n in range(cross_k):
            a = pd.read_csv(f"../Results/results_{name}/partition{n + 1}Cross.csv")
            f.write("\\hline\\hline\n")
            f.write("\\textbf{Fold " + str(n + 1) + "} & & \\\ \n")
            for i, elem in a.iterrows():
                xd = elem.astype(str).values[:-2]
                xd[-1] = xd[-1] + "\%"
                xd[-2] = xd[-2] + "\%"
                f.write("\\hspace{1.5mm} " + " & ".join(xd) + " \\\ \n")
            accsTrain.append(elem.values[-2])
            accsTest.append(elem.values[-1])

        f.write("Accuracies train: " + " ".join([str(acc) for acc in accsTrain]) + "\n")
        f.write("Accuracy train mean:" + str(np.mean(accsTrain)) + "  std:" + str(np.std(accsTrain)))
        f.write("Accuracies test: " + " ".join([str(acc) for acc in accsTest]) + "\n")
        f.write("Accuracy test mean:" + str(np.mean(accsTest)) + "  std:" + str(np.std(accsTest)))
    f.close()


def mergedRulesLatex(name):
    """
    Function that creates the set of rules merged among folds to be added to latex tables. Notice that the function
    crossValidationExperiment has to be called before in order to have the needed documents.

    :param name: The name of the dataset.
    """
    a = pd.read_csv(f"../Results/results_{name}/results{name}Cross.csv")
    uniqueRules = a["Rule"].unique()
    with open(f"../Results/results_{name}/combLatex.txt", 'w', encoding="utf-8") as f:
        for i, rule in enumerate(uniqueRules):
            grouped = a[a["Rule"] == rule].groupby(by="Partition")
            res = [str(i + 1), rule]
            for name, group in grouped:
                b = group[["Precision", "Recall"]].mean()
                res.append(str(round(b["Precision"], 2)) + "/" + str(round(b["Recall"], 2)))
            f.write(" & ".join(res) + " \\\ \n")
    f.close()

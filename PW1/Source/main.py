import time
from classes.CN2 import CN2
from experiments import crossValidationExperiment, allRulesLatex, mergedRulesLatex


def main():
    print("--------------------- LENSES DATASET ---------------------")
    starTime = time.time()
    cn2 = CN2(k=3, min_significance=0.6, verbose=2)
    cn2.fit("../Data/lenses.csv", splitTest=0, header='infer')
    endTime = time.time()
    timeDiff = endTime - starTime
    print(f"\nComputed in {round(timeDiff % 60, 2)} seconds\n\n")

    print("---------------------- IRIS DATASET ----------------------")
    starTime = time.time()
    cn2 = CN2(k=3, min_significance=1, verbose=2)
    cn2.fit("../Data/iris.csv", numCols=["sepal length", "sepal width", "petal length", "petal width"],
            colNames=["sepal length", "sepal width", "petal length", "petal width"])
    endTime = time.time()
    timeDiff = endTime - starTime
    print(f"\nComputed in {round(timeDiff % 60, 2)} seconds\n\n")

    print("---------------------- CAR DATASET ----------------------")
    starTime = time.time()
    cn2 = CN2(k=3, min_significance=0.6, verbose=2)
    cn2.fit("../Data/car.csv", colNames=["buying", "maint", "doors", "persons", "lug_boot", "safety"])
    endTime = time.time()
    timeDiff = endTime - starTime
    print(f"Computed in {round(timeDiff // 60, 2)} minutes and {round(timeDiff % 60, 2)} seconds\n\n")
    #
    print("-------------------- NURSERY DATASET --------------------")
    starTime = time.time()
    cn2 = CN2(k=3, min_significance=1, verbose=2)
    cn2.fit("../Data/nursery.csv", colNames=["parents", "has_nurs", "form", "children",
                                          "housing", "finance", "social", "health"])
    endTime = time.time()
    timeDiff = endTime - starTime
    print(f"Computed in {round(timeDiff // 60, 2)} minutes and {round(timeDiff % 60, 2)} seconds\n\n")

    if False:  # Experiments
        cross_k = 5
        # Iris
        crossValidationExperiment("../Data/iris.csv", 3, 1, cross_k=cross_k,
                                  numCols=["sepal length", "sepal width", "petal length", "petal width"],
                                  colNames=["sepal length", "sepal width", "petal length", "petal width"])
        allRulesLatex("iris", cross_k=cross_k)
        mergedRulesLatex("iris")

        # Car
        crossValidationExperiment("../Data/car.csv", 3, 0.6, cross_k=cross_k,
                                  colNames=["buying", "maint", "doors", "persons",
                                            "lug_boot", "safety"])
        allRulesLatex("car", cross_k=cross_k)
        mergedRulesLatex("car")

        # Nursery
        crossValidationExperiment("../Data/nursery.csv", 3, 1, cross_k=cross_k,
                                  colNames=["parents", "has_nurs", "form", "children",
                                            "housing", "finance", "social", "health"])
        allRulesLatex("nursery", cross_k=cross_k)
        mergedRulesLatex("nursery")


if __name__ == "__main__":
    main()

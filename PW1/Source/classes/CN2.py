import itertools
import statistics
import numpy as np
import pandas as pd
from classes import Rule, Complex
from sklearn.metrics import accuracy_score, precision_score
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split


class CN2:
    def __init__(self, k, min_significance, seed=42, verbose=0):
        """
        Method called in the initialization of the CN2 algorithm. Defines the parameters.

        :param k: The k value that indicates the amount of complexes to maintain in each iteration.
        :param min_significance: The min significance to consider while performing the specialization process.
        :param seed: The seed to be used, will only be used by the train_test_split sklearn's function.
        :param verbose: The verbose indicator. 0 used for no prints, 1 some of them, 2 all of them.
        """
        self.k = k
        self.min_significance = min_significance
        self.seed = seed
        self.verbose = verbose
        self.rules = None
        self.trainX = None
        self.trainY = None
        self.testX = None
        self.testY = None
        self.true_probs = None
        self.all_single_selectors = None

    def prepareData(self, path, sep=',', classCol=-1, header=None, colNames=None,
                    numCols=[], binCols=[], disc=5, splitTest=0.33):
        """
        This function prepare the data of the given path taking into consideration the parameters.

        :params: Check the fit function to find the parameters' explanation.
        :return: The read, preprocessed and split dataset. If splitTest=0, trainY==trainX and testY==testX.
        """
        X, Y = readAndPreprocess(path, sep=sep, classCol=classCol, header=header,
                                 colNames=colNames, numCols=numCols, binCols=binCols, disc=disc)
        if self.verbose == 2:
            print(f"Dataset {path.split('/')[-1][:-4]} has a total of {len(X)} instances\n")
        if splitTest:
            self.trainX, self.testX, self.trainY, self.testY = train_test_split(X, Y, test_size=splitTest,
                                                                                random_state=self.seed, stratify=Y)
        else:
            self.trainX = X
            self.testX = X
            self.trainY = Y
            self.testY = Y

        self.trainX.reset_index(inplace=True, drop=True)
        self.testX.reset_index(inplace=True, drop=True)

        return self.trainX, self.testX, self.trainY, self.testY

    def _compute_initial_selectors(self):
        """
        Private method to obtain the initial set of selectors for all the features

        :return: Initial set of selectors
        """
        self.all_single_selectors = []
        for col in self.trainX.columns:
            for label in np.unique(self.trainX[col]):
                complex_obj = Complex.Complex()
                complex_obj.add_selector(col, label)
                self.all_single_selectors.append(complex_obj)
        return self.all_single_selectors

    def get_rules(self):
        """
        Rules getter.

        :return: The rules of the CN2 algorithm.
        """
        return self.rules

    def fit(self, path, sep=',', classCol=-1, header=None, colNames=None,
            numCols=[], binCols=[], disc=5, splitTest=0.33):
        """
        Fit function that reads and preprocess a CSV, creates the rules of the CN2 algorithm
        and then evaluate them with the train and test splits/partitions.

        :param path: The path were the CSV is located.
        :param sep: The way of separating the read CSV of the path.
        :param classCol: Indicates the column of the CSV were the class label are located.
        :param header: Is it used in function read_csv to define if the CSV has header or not.
        :param colNames: The names of the columns to be used. If none [col_0, col_1, ...] will be used.
        :param numCols: The numerical columns of the CSV that have to be discretized using the disc parameter.
        :param binCols: The binary columns of the CSV that have to be discretized with 2 bins.
        :param disc: The discretization parameter that will be used in numCols.
        :param splitTest: The percentage of the test split that will be performed.
        :param delFirstCol: Indicate whether to delete the first
        :return: The train and test accuracies with the generated rules.
        """
        self.trainX, self.testX, self.trainY, self.testY = self.prepareData(path, sep=sep, classCol=classCol,
                                                                            header=header, colNames=colNames,
                                                                            numCols=numCols, binCols=binCols,
                                                                            disc=disc, splitTest=splitTest)
        self.true_probs = self.trainY.value_counts(normalize=True)
        self.all_single_selectors = self._compute_initial_selectors()
        self.trainX['class_label'] = self.trainY.values
        self.rules = []

        # Main algorithm function
        self._execute()

        trainScore, _, _ = self.score(self.trainX, self.trainY, train=True)
        testScore, _, _ = self.score(self.testX, self.testY, train=False) if splitTest else (None, _, _)
        return trainScore, testScore

    def _execute(self):
        """
        Main loop of the CN2 algorithm. Iteratively find the best complex and add the rule to the set of rules while
        reducing the covered instances. If the verbose parameter is set to 2, all the rules will be shown.
        """
        instances = self.trainX
        instances_indices = set(list(range(len(instances))))
        iteration = 1
        best_complex, instances_covered, instances_indices_covered = self._find_best_complex(instances)
        while best_complex and instances_indices:
            class_label = statistics.mode(instances_covered['class_label'])
            best_rule = Rule.Rule(best_complex, class_label, len(instances_covered))
            self.rules.append(best_rule)
            instances_indices -= instances_indices_covered
            instances = self.trainX.iloc[list(instances_indices)]
            if self.verbose == 2:
                print(f"Rule {iteration}: {best_rule}")
                if instances.empty:
                    print(f"  All the instances are covered\n")
                else:
                    print(f"  There are still {len(instances)} non covered instances\n")
            if instances.empty:
                break
            best_complex, instances_covered, instances_indices_covered = self._find_best_complex(instances)
            iteration += 1

        if instances_indices:
            instances = self.trainX.iloc[list(instances_indices)]
            class_label = statistics.mode([instance[-1] for instance in instances.values])
            best_rule = Rule.Rule(Complex.Complex(), class_label, len(instances_indices))
            self.rules.append(best_rule)
            if self.verbose == 2:
                print(f"Rule {iteration}: {best_rule}")
                print(f"Used last condition with {len(instances_indices)} instances set to class = {class_label}\n")

        print(f"Total of {len(self.rules)} rules")

    def _find_best_complex(self, instances):
        """
        Function that find the best complex, and its characteristics, for a given set of instances.

        :param instances: The instances to find the best complex for.
        :returns:
            The best found complex.
            The instances covered by the best complex.
            The indices of the covered instances.
        """
        star = [Complex.Complex()]
        best_complex = None
        best_complex_entropy = np.inf
        best_complex_significance = 0
        best_complex_covered_instances = None
        best_complex_covered_indices_instances = None
        best_complex_covered_num = 0
        while len(star):
            new_star = set(itertools.product(star, self.all_single_selectors))
            new_star = set([merge_complexes(cp1, cp2) for cp1, cp2 in new_star])
            new_star = [complex_obj.compute_complex_scores(instances, self.true_probs)
                        for complex_obj in new_star if not complex_obj.is_null]
            new_star = [complex_obj for complex_obj in new_star if not complex_obj.is_null]
            new_star.sort(key=lambda c: (c.complex_entropy, -c.covered_instances_num,
                                         -c.complex_significance, c.complex_size))

            if len(new_star):
                best_complex_new_star = new_star[0]

                if best_complex_new_star.complex_significance >= self.min_significance:
                    if best_complex_new_star.complex_entropy <= best_complex_entropy:
                        if best_complex_new_star.complex_entropy < best_complex_entropy or \
                                best_complex_new_star.covered_instances_num > best_complex_covered_num or \
                                best_complex_new_star.complex_significance > best_complex_significance:
                            best_complex = best_complex_new_star
                            best_complex_entropy = best_complex_new_star.complex_entropy
                            best_complex_significance = best_complex_new_star.complex_significance
                            best_complex_covered_instances = best_complex_new_star.covered_instances
                            best_complex_covered_indices_instances = best_complex_new_star.covered_instances_indices
                            best_complex_covered_num = best_complex_new_star.covered_instances_num

            star = new_star[:self.k]

        if best_complex_covered_indices_instances is not None:
            best_complex_covered_indices_instances = set(best_complex_covered_indices_instances)
            if self.verbose == 2:
                print(f"Entropy = {round(best_complex_entropy, 4)}"
                      f" and Significance = {round(best_complex_significance, 4)}")

        return best_complex, best_complex_covered_instances, best_complex_covered_indices_instances

    def score(self, X, Y, train=False, rules=None):
        """
        Function that computes the score of a given dataset with the induced set of rules
        (or the ones given in rules parameter). If the rules are given, this function work
        as a rule interpreter for any kind of dataset and set of rules.

        :param X: The features for the ones to inference the score with the rules of the class.
        :param Y: The real classes of the features, to compute the precision, recall and accuracy.
        :param train: Boolean that indicates if we are using the train or test dataset.
        For the train we compute precision and recall for each of the rules.
        :param rules: An optional set of rules to test the given datasets. If None, the classes induced rules are used.
        :return: The accuracy of the rules and the precisions and recalls for each one of the rules.
        """
        if X.empty:
            return None

        if not rules:
            assert self.rules, "Can't evaluate the dataset without a previous fit()"
            rules = self.rules

        Y = Y.astype(str)
        predictedClasses = np.zeros_like(Y.values, dtype=np.dtype('U100'))
        featuresCopy = X.copy()
        precisions = []
        recalls = []
        for idx, rule in enumerate(rules):
            complex_obj = rule.get_complex()
            predictedLabel = str(rule.get_class_label())
            coveredInstances = complex_obj.compute_covered_instances(featuresCopy)
            indices_to_drop = list(coveredInstances.index.values)
            predictedClasses[indices_to_drop] = predictedLabel
            featuresCopy.drop(indices_to_drop, inplace=True)

            if train:
                true = np.array(Y.iloc[indices_to_drop].values)
                pred = np.array(predictedClasses[indices_to_drop])
                precision = precision_score(true, pred, labels=[predictedLabel], average='micro', zero_division=0)
                recall = sum(true == pred) / len(Y[Y == predictedLabel])
                precisions.append(precision)
                recalls.append(recall)
                if self.verbose == 2:
                    print(f"Rule {idx + 1}:  {rule};   Precision = {round(precision * 100, 2)}%; "
                          f"Recall = {round(recall * 100, 2)}%")

        if train:
            precisions = np.array(precisions)
            recalls = np.array(recalls)
            F1 = 2 * np.multiply(precisions, recalls) / (precisions + recalls)

            if self.verbose:
                print("\nBest 5 rules F1-score:")
                for index in np.argsort(-F1)[:5]:
                    print(f"  Rule {index+1} with {round(F1[index] * 100, 2)}%")
                print()

        predictedClasses[predictedClasses == "0"] = "nan"
        score = accuracy_score(Y.astype(str).values, predictedClasses) * 100
        print(f"Accuracy on {'train' if train else 'test'} dataset = {round(score, 2)}%")

        return score, precisions, recalls


def merge_complexes(complex1, complex2):
    complex_obj = Complex.Complex()
    selectors1 = complex1.get_selectors()
    selectors2 = complex2.get_selectors()
    if set(selectors1.keys()).isdisjoint(selectors2.keys()):
        for selector1Key in selectors1:
            for selector1 in selectors1[selector1Key]:
                complex_obj.add_selector_object(selector1Key, selector1)
        for selector2Key in selectors2:
            for selector2 in selectors2[selector2Key]:
                complex_obj.add_selector_object(selector2Key, selector2)
    return complex_obj


def readAndPreprocess(path, sep=',', classCol=-1, header=None,
                      colNames=None, numCols=[], binCols=[], disc=5):
    dataframe = pd.read_csv(path, header=header, sep=sep, engine='python')
    if header != 'infer':
        if not colNames:
            dataframe.columns = ["col_" + str(col) for col in range(len(dataframe.columns))]
        else:
            dataframe.columns = colNames + ["class_label"]
    features = dataframe.drop(dataframe.columns[classCol], axis=1)
    classes = dataframe[dataframe.columns[classCol]]
    if len(numCols):
        features[numCols] = pd.DataFrame(MinMaxScaler(copy=False).fit_transform(features[numCols]), columns=numCols)
        for col in numCols:
            features[col] = pd.cut(features[col], bins=disc)
    for col in binCols:
        features[col] = pd.cut(features[col], bins=2)

    return features, classes

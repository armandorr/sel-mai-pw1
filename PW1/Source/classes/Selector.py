class Selector:
    def __init__(self, variable, class_label):
        self.variable = variable
        self.class_label = class_label

    def __str__(self):
        return f"{self.variable} = {self.class_label}"

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        return self.variable == other.variable and self.class_label == other.class_label

    def get_variable(self):
        return self.variable

    def get_class_label(self):
        return self.class_label

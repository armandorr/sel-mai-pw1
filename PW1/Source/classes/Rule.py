class Rule:
    def __init__(self, complex_obj, class_label, num_instances_covered):
        self.complex_obj = complex_obj
        self.class_label = class_label
        self.num_instances_covered = num_instances_covered

    def __str__(self):
        return f"IF {self.complex_obj} THEN {self.class_label}"

    def __repr__(self):
        return self.__str__()

    def get_complex(self):
        return self.complex_obj

    def get_class_label(self):
        return self.class_label

import numpy as np
from classes import Selector
from scipy.stats import entropy


class Complex:
    def __init__(self):
        self.selectors = {}
        self.is_null = True  # Because is empty at the start
        self.covered_instances = None
        self.covered_instances_indices = None
        self.complex_size = 0

    def __str__(self):
        if not self.selectors:
            result = "Default"
        else:
            result = " AND ".join([selector.__str__()
                                   for selectorKey, selectorValue in self.selectors.items()
                                   for selector in selectorValue])
        return result

    def __repr__(self):
        return self.__str__()

    def __hash__(self):
        tup = ()
        for key in self.selectors:
            tup += (key,)
        tup += (self.is_null,)
        return hash(tup)

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        if self.is_null != other.is_null:
            return False
        return self.selectors == other.selectors

    def add_selector(self, variable, class_label):
        self.is_null = variable in self.selectors
        if not self.is_null:
            self.selectors[variable] = []
        self.selectors[variable].append(Selector.Selector(variable, class_label))

    def add_selector_object(self, variable, selector):
        self.is_null = variable in self.selectors
        if not self.is_null:
            self.selectors[variable] = []
        self.selectors[variable].append(selector)
        self.complex_size += 1

    def get_selectors(self):
        return self.selectors

    def compute_complex_scores(self, instances, true_probs):
        self.compute_covered_instances(instances)
        self._compute_entropy_and_significance(true_probs)
        return self

    def check_instance_covered(self, instance):
        for instanceName, instanceValue in instance.items():
            if instanceName in self.selectors:
                selector = self.selectors[instanceName][0]
                if instanceValue != selector.class_label:
                    return False
        return True

    def compute_covered_instances(self, instances):
        self.covered_instances = instances.loc[instances.apply(self.check_instance_covered, axis=1)]
        if len(self.covered_instances) == 0:
            self.is_null = True
        self.covered_instances_indices = self.covered_instances.index
        return self.covered_instances

    def _compute_entropy_and_significance(self, true_probs):
        covered_probs = self.covered_instances['class_label'].value_counts(normalize=True)
        self.complex_entropy = entropy(covered_probs, base=2)
        self.complex_significance = 2 * np.sum(covered_probs * np.log(covered_probs / true_probs))
        self.covered_instances_num = len(self.covered_instances)

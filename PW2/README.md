# Combining multiple classifiers
## Decision Forests and Random Forests
Supervised and Experiential Learning (SEL) - Master in Artificial Intelligence

## Author
- Armando Rodriguez Ramos

## Running script for the first time
These sections show how to create virtual environment for
our script and how to install dependencies. The first thing to do is to install Python 3.9.
1. Open folder in terminal
```bash
cd <root_folder_of_project>/
```
2. Create virtual env
```bash
python3 -m venv venv/
```
3. Open virtual env
```bash
source venv/bin/activate
```
4. Install required dependencies
```bash
pip install -r requirements.txt
```
## Execute code
1. Open Source folder in terminal
```bash
cd Source/
```
2. Running the main code
- You can modify the parameters on the main function. Some of the things that can be done will be:
  - Select the classifier method with *randomForest* parameter (True = Random Forest, False = Decision Tree).
  - Select the visualization options that better fits you:
    - *treeToText*: Show the tree in the terminal in a string fashion.
    - *treeToPNG*: Create and save the best trees in PNG format using Graphviz. As this module usually give problems to install I deleted it from the requeriments.txt. Make sure to uncomment line 3 of treeToGraph.py and install *graphviz~=0.20.1* if you want to use this option.
  - The latex tables were created using *treeToLatex* = True.
  - Select the desired datasets to use with the *datasets* dictionary. Add any other dataset by adding the name into this dictionary. Then include the *elif* statement calling the *prepareData* function taking the correct path to it.
  - The number of trees or the number of random features can be modified by changing the *NTS* and *FS* arrays respectively. 
  - Vary the verbose parameter on line 51 for each of the methods in order to see the execution process and the last tree of each run (verbose=1) or all the trees of each method (verbose=2).

- By default, the main function will execute all the datasets of the report, with the indicated parameters and the minimum level of verbose, i.e., not showing any tree. 
 ```bash
 python3 main.py
 ```
3. Close virtual environment
```bash
deactivate
```
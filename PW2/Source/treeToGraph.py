import ast
import numpy as np
# from graphviz import Digraph

colorsList = ["lightblue", "lightgoldenrodyellow", "lightcoral", "lightgreen", "lightsalmon", "lightpink",
              "lightskyblue", "lightcyan", "lavender", "mediumpurple", "rosybrown", "orchid", "red", "blue", "green"]


def createTree(graph, node, colorDict, level="", parentNode=None, edgeName=None):
    """Recursively create the graph."""
    feature = node[0]
    currentNodeName = feature+str(level)
    shape = "box" if len(node) > 1 else "ellipse"
    graph.node(currentNodeName, label=feature, style='filled', shape=shape,
               fillcolor=colorDict[feature] if feature in colorDict else None)
    if parentNode is not None:
        graph.edge(parentNode, currentNodeName, label=edgeName, aplines='line')
    if len(node) > 1:
        createTree(graph, node[3][0], colorDict, level=level+str(0), parentNode=currentNodeName, edgeName=node[1])
        createTree(graph, node[4][0], colorDict, level=level+str(1), parentNode=currentNodeName, edgeName=node[2])


def treeToGraphviz(tree_string, featuresNames, savePath):
    """Converts a decision tree in string format to a Graphviz graph."""
    colorDict = {f: c for f, c in zip(np.unique(featuresNames), colorsList)}
    tree = ast.literal_eval(tree_string)
    graph = Digraph()
    createTree(graph, tree, colorDict)
    graph.graph_attr['dpi'] = '300'
    graph.render(savePath, format="png", cleanup=True)

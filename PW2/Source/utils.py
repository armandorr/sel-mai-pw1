import numpy as np
import pandas as pd
from scipy import stats
from collections import Counter
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix


def prepareData(path, sep=',', classCol=-1, header='infer', colNames=None, splitTest=0.33, splitSeed=42):
    """
    This function prepare the data of the given path taking into consideration the parameters.

    :param path: The path were the CSV is located.
    :param sep: The way of separating the read CSV of the path.
    :param classCol: Indicates the column of the CSV were the class label are located.
    :param header: Is it used in function read_csv to define if the CSV has header or not.
    :param colNames: The names of the columns to be used. If none [col_0, col_1, ...] will be used.
    :param splitTest: The percentage of the test split that will be performed.
    :param splitSeed: The seed to use when splitting the data
    :return: The read, preprocessed and split dataset. If splitTest=0, trainY==trainX and testY==testX.
    """
    X, Y = _readAndPreprocess(path, sep=sep, classCol=classCol, header=header,
                              colNames=colNames)

    print(f"Dataset {path.split('/')[-1][:-4]} has a total of {len(X)} instances\n")
    if splitTest:
        trainX, testX, trainY, testY = train_test_split(X, Y, test_size=splitTest,
                                                        random_state=splitSeed, stratify=Y)
    else:
        trainX, testX = X, X
        trainY, testY = Y, Y

    trainX.reset_index(inplace=True, drop=True)
    testX.reset_index(inplace=True, drop=True)

    return trainX, testX, trainY, testY


def _readAndPreprocess(path, sep=',', classCol=-1, header='infer', colNames=None):
    dataframe = pd.read_csv(path, header=header, sep=sep, engine="python")

    if header != 'infer':
        if not colNames:
            dataframe.columns = ["col_" + str(col) for col in range(len(dataframe.columns))]
        else:
            dataframe.columns = colNames + ["true_label"]
    features = dataframe.drop(dataframe.columns[classCol], axis=1)
    classes = dataframe[dataframe.columns[classCol]]
    return features, classes


def computeGini(datasetY):
    """
    Compute the gini value of a dataset.
    """
    return 1 - np.sum(datasetY.value_counts(normalize=True) ** 2)


def computeGinitPartition(dataset1, dataset2):
    """
    Compute the combined gini value of two datasets.
    """
    weight = len(dataset1) / (len(dataset1) + len(dataset2))
    return weight * computeGini(dataset1.iloc[:, -1]) + (1 - weight) * computeGini(dataset2.iloc[:, -1])


def computeScoreTree(forest, X, Y):
    """
    Compute the score for a forest given the X and Y datasets.
    """
    trueLabels = [str(label) for label in Y.values]
    predictions = np.empty((len(X), len(forest.trees)), dtype='object')
    accuracies = []
    for treeIdx, innerTree in enumerate(forest.trees):
        predLabels = innerTree.predict(X).sort_index().values
        predictions[:, treeIdx] = predLabels
        accuracies.append(accuracy_score(trueLabels, predLabels))

    predLabels, _ = stats.mode(predictions, axis=1, keepdims=False)
    predLabels = [str(label) for label in predLabels]

    confusionMatrix = confusion_matrix(predLabels, trueLabels)
    accuracy = accuracy_score(trueLabels, predLabels)

    return confusionMatrix, accuracy, np.argmax(accuracies), np.max(accuracies)


def computeFeatureImportance(forest):
    """
    Compute the feature importance for a forest.
    """
    allFeatures = dict.fromkeys(forest.features, 0)
    featureImportance = allFeatures.copy()
    for treeIdx, innerTree in enumerate(forest.trees):
        featureImportance = Counter(featureImportance)
        featureImportance.update(Counter(innerTree.getFeatureImportance(allFeatures)))
        featureImportance = dict(featureImportance)

    totalValue = sum(featureImportance.values())
    for key in featureImportance:
        featureImportance[key] = round(featureImportance[key]/totalValue, 3) if totalValue else 0

    return featureImportance

import math
import time
import numpy as np
from utils import prepareData
from classes.DecisionForest import DecisionForest
from classes.RandomForest import RandomForest
from treeToGraph import treeToGraphviz


def main():
    randomForest = False
    treeToLatex = False
    treeToPNG = False
    treeToText = False
    nExperiments = 1
    
    assert nExperiments > 0, "Use, at least, 1 experiment"

    datasets = {"ecoli": True, "breast": False, "nursery": False}

    print(f"--------------------- {'RANDOM FOREST' if randomForest else 'DECISION FOREST'} ---------------------\n")

    for datasetName in [name for name, useBool in datasets.items() if useBool]:
        if datasetName == "ecoli":
            print("-------------------- ECOLI DATASET --------------------")
            trainX, testX, trainY, testY = prepareData("../Data/ecoli.csv", splitTest=0.33)

        elif datasetName == "breast":
            print("-------------------- BREAST DATASET --------------------")
            trainX, testX, trainY, testY = prepareData("../Data/breast.csv", splitTest=0.33)

        elif datasetName == "nursery":
            print("-------------------- NURSERY DATASET --------------------")
            trainX, testX, trainY, testY = prepareData("../Data/nursery.csv", splitTest=0.33)

        else:
            print(f"Dataset {datasetName} not found. Skipping it.")
            continue

        trainDF = trainX.copy()
        trainDF['true_label'] = trainY.values

        M = len(trainX.columns)
        NTS = [1, 10, 25, 50, 75, 100]
        if randomForest:
            FS = [1, 2, int(math.log2(M) + 1), int(math.sqrt(M))]
        else:
            FS = [int(M / 4), int(M / 2), int(3 * M / 4), "uniform"]

        firstTime = True
        index = 0
        for NT in NTS:
            for F in FS:
                results = np.zeros((nExperiments, 3))
                for nExperiment in range(nExperiments):
                    starTime = time.time()
                    forest = RandomForest(F=F, NT=NT, verbose=0) if randomForest else DecisionForest(F=F, NT=NT, verbose=0)
                    forest.fit(trainDF)
                    endTime = time.time()
                    timeDiff = endTime - starTime

                    featureImportance = forest.getFeatureImportance()
                    _, accTrain, _, _ = forest.score(trainX, trainY)
                    matrixTest, accTest, bestTree, bestAcc = forest.score(testX, testY)

                    results[nExperiment, :] = [accTrain, accTest, timeDiff]

                accTrain = np.average(results[:, 0])
                accTest = np.average(results[:, 1])
                timeDiff = np.average(results[:, 2])

                F = "U" if isinstance(F, str) else F

                if treeToPNG:
                    treeToGraphviz(forest.trees[bestTree].__str__(conversionMode=True), featuresNames=trainY.values,
                                savePath=f"../Results/{datasetName}/{'RF' if randomForest else 'DF'}_{index}_"
                                            f"nt{NT}_f{F}_{bestTree}")
                    index += 1

                if treeToText:
                    print(forest.trees[bestTree])

                if treeToLatex:
                    if firstTime:
                        print("\\textbf{NT} & \\textbf{F} & \\textbf{TrA} & \\textbf{TeA} & \\textbf{Time} & ", end="")
                        print(" & ".join(["\\textbf{"+str(key)+"}" for key in featureImportance.keys()]) + "\\\\")
                        print("\\hline")
                        firstTime = False
                    print(f"{NT} & {F} & {round(accTrain * 100)}\\% & {round(accTest * 100)}\\% & ", end="")
                    if timeDiff//60 >= 1:
                        print(f"{round(timeDiff//60)}m {round(timeDiff % 60)}s & ", end="")
                    else:
                        print(f"{round(timeDiff % 60, 2)}s & ", end="")
                    print(" & ".join([str(round(value, 3)) for value in featureImportance.values()]) + "\\\\")
                else:
                    print(f"NT={NT} & F={F} =>")
                    print(f"  Average accuracy train = {round(accTrain * 100)}%")
                    print(f"  Average accuracy test = {round(accTest * 100)}%")
                    print("  Confusion matrix test:")
                    print('  ' + str(matrixTest).replace('\n', '\n  '))
                    print("  Feature importance:", featureImportance)
                    print(f"  Fitted in average in {round(timeDiff % 60, 2)} seconds\n\n")


if __name__ == "__main__":
    main()

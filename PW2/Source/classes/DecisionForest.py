import numpy as np
from classes import Tree
from utils import computeScoreTree, computeFeatureImportance


class DecisionForest:
    def __init__(self, F, NT, seed=42, verbose=0):
        """
        Method called in the initialization of the Decision Forest algorithm. Defines the parameters.

        :param F: Number of random features used in the different trees.
        :param NT: The number of trees.
        :param seed: The seed to be used, will only be used.
        :param verbose: The verbose indicator. 0 used for no prints, 1 some of them, 2 all of them.
        """
        assert F == "uniform" or isinstance(F, int), "F must be an integer or the string 'uniform'"
        self.F = F
        self.NT = NT
        self.verbose = verbose

        if seed:
            np.random.seed(seed)

        self.features = []
        self.trees = []

        self.testX = None
        self.testY = None

    def fit(self, trainDF):
        """
        Fit the Decision Forest algorithm with the given train dataset.

        :param trainDF: The dataset to fit the model with.
        """
        self.features = list(trainDF.columns[:-1])
        FS = np.random.randint(1, len(self.features), size=self.NT)

        for nTree in range(self.NT):
            # Select F random features for each tree
            F = FS[nTree] if self.F == "uniform" else self.F
            F = min(F, len(self.features))
            XCols = list(trainDF[self.features].sample(n=F, axis='columns').columns) + [trainDF.columns[-1]]
            X = trainDF.loc[:, XCols]

            # Create the tree
            tree = Tree.Tree(X, seed=np.random.randint(1e+6))
            self.trees.append(tree)

            if self.verbose == 2 or (self.verbose == 1 and nTree+1 == self.NT):
                print(f"Tree number {nTree + 1} using columns {XCols[:-1]}:")
                print(tree)

    def score(self, X, Y):
        """
        Score the Decision Forest model. Predicts with the X and score with Y.

        :param X: Dataset to predict with.
        :param Y: Dataset of the classes of the instances in X.
        """
        assert self.trees, "Please fit the tree before calling to this function"
        return computeScoreTree(self, X, Y)

    def getFeatureImportance(self):
        """
        Compute the feature importance for this Decision Forest model.
        """
        assert self.trees, "Please fit the tree before calling to this function"
        return computeFeatureImportance(self)

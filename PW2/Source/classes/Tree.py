import numpy as np
import pandas as pd
from statistics import mode
from itertools import combinations
from utils import computeGini, computeGinitPartition


class Tree:
    def __init__(self, trainDF, F=None, seed=None):
        """
        Method called in the initialization of a Tree. Recall that a tree has two subtrees, or it is a leaf.
        Defines the parameters.

        :param trainDF: The dataset for which the tree is created.
        :param F: The random number of features to split with (optional). All features used if F=None.
        :param seed: The seed to be used.
        """
        self.trainDF = trainDF
        self.F = F  # If using F we are in randomForest

        # Representation of the Tree (Node)
        self.falseTree = None
        self.trueTree = None
        self.isLeaf = False
        self.label = None

        # Information of split
        self.splitGini = None
        self.splitFeature = None
        self.splitValue = None
        self.splitNumerical = False
        self.bestTrueSplit = None
        self.bestFalseSplit = None

        if seed:
            np.random.seed(seed)

        self._fit()

    def _fit(self):
        """
        Fit the Tree with the class dataset. It will create two subtrees or will stop if we are in a leaf.
        """
        if len(self.trainDF.iloc[:, -1].unique()) == 1:
            self.isLeaf = True
            self.label = self.trainDF.iloc[0, -1]
        else:
            gini = computeGini(self.trainDF.iloc[:, -1])
            self._findBestSplit()
            if self.splitGini is not None and gini != self.splitGini:  # Else we have finished the training of this Tree
                self.trueTree = Tree(self.bestTrueSplit, self.F, seed=np.random.randint(1e+6))
                self.falseTree = Tree(self.bestFalseSplit, self.F, seed=np.random.randint(1e+6))
            else:
                self.isLeaf = True
                self.label = mode(self.trainDF.iloc[:, -1].values)

    def _findBestSplit(self):
        """
        Auxiliary function that finds the best split for the class dataset.
        """
        features = list(self.trainDF.columns[:-1])
        if self.F:
            features = np.random.choice(features, size=min(self.F, len(features)), replace=False)

        for feature in features:
            if self.trainDF[feature].dtype != 'object':
                sValues = self.trainDF[feature].sort_values().unique()
                for pValue, nValue in zip(sValues[:-1], sValues[1:]):
                    midValue = (pValue + nValue) / 2
                    self._updateBestSplit(midValue, feature, True)
            else:
                values = self.trainDF[feature].unique()
                subsets = [comb for num in range(len(values)) for comb in
                           [set(tup) for tup in combinations(values, num)]]
                for subset in subsets[1:len(subsets) // 2 + 1]:  # We take out the empty set
                    self._updateBestSplit(subset, feature, False)

    def _updateBestSplit(self, value, feature, numerical):
        """
        Auxiliary function that checks and update the best split using the value and feature parameters.
        """
        rows = self.trainDF[feature] >= value if numerical else self.trainDF[feature].isin(value)
        truePartition = self.trainDF[rows]
        falsePartition = self.trainDF[~rows]
        if len(truePartition) == 0 or len(falsePartition) == 0:
            return
        gini = computeGinitPartition(truePartition, falsePartition)
        if (self.splitGini is None) or gini < self.splitGini:
            self.splitGini, self.splitFeature = gini, feature
            self.splitValue, self.splitNumerical = value, numerical
            self.bestTrueSplit, self.bestFalseSplit = truePartition, falsePartition

    def predict(self, X):
        """
        Auxiliary function that predicts in a recursive way given the X dataset.
        """
        if self.isLeaf:
            return pd.Series(len(X) * [self.label], index=list(X.index))
        feature, value, numerical = self.splitFeature, self.splitValue, self.splitNumerical
        rows = X[feature] >= value if numerical else X[feature].isin(value)
        truePartition = X[rows]
        falsePartition = X[~rows]
        trueDf = self.trueTree.predict(truePartition)
        falseDf = self.falseTree.predict(falsePartition)
        return pd.concat([trueDf, falseDf])

    def getFeatureImportance(self, allFeatures):
        """
        Auxiliary recursive function that compute the amount of features of each kind in the whole tree.
        """
        if not self.isLeaf:
            allFeatures[self.splitFeature] += 1
            allFeatures = self.trueTree.getFeatureImportance(allFeatures)
            allFeatures = self.falseTree.getFeatureImportance(allFeatures)
        return allFeatures

    def __str__(self, conversionMode=False, indent=1):
        """
        String representation of the Tree object. Use conversionMode=True to obtain the Graphviz visualization and
        conversionMode=False to obtain the terminal representation instead.
        """
        if conversionMode:
            tree = "["
            if self.isLeaf:
                tree += '"'+str(self.label)+'"'
            else:
                operator = ' >= ' if self.splitNumerical else " == "
                notOperator = " < " if self.splitNumerical else " != "
                value = round(self.splitValue, 3) if self.splitNumerical else "["+", ".join(list(self.splitValue))+"]"
                tree += '"'+self.splitFeature+'","'+operator+str(value)+'","'+notOperator+str(value)+'",'
                tree += "["+self.trueTree.__str__(conversionMode)+"],["+self.falseTree.__str__(conversionMode)+"]"
            tree += "]"
        else:
            indentStr = "   ".join(indent * ["|"])
            tree = ""
            if self.isLeaf:
                tree += indentStr + "--- class: " + str(self.label) + "\n"
            else:
                operator = " ≥ " if self.splitNumerical else " ∈ "
                value = round(self.splitValue, 3) if self.splitNumerical else "["+", ".join(list(self.splitValue))+"]"
                tree += indentStr + "--- " + self.splitFeature + operator + str(value) + "\n"
                tree += self.trueTree.__str__(conversionMode, indent=indent + 1)
                notOperator = " < " if self.splitNumerical else " ∉ "
                tree += indentStr + "--- " + self.splitFeature + notOperator + str(value) + "\n"
                tree += self.falseTree.__str__(conversionMode, indent=indent + 1)
        return tree

for i in {100..1000..100}
do
	echo "In progress with $i..." 
	cd data/
	python3 groupCSV.py
	cd ../
	python3 massTesting.py $i > results/massTesting$i.txt
	echo "Done"
done

import pandas as pd
import numpy as np
import math
from scipy import stats as st

def info_measures(ml, gr, garnish):
    """
    Obtain measures information to have a representative value for the ingredient.

    :param ml: milliliters of the ingredient in all the recipes it appears.
    :param gr: grams of the ingredient in all the recipes it appears.
    :param garnish: type of garnish of the ingredient in all the recipes it appears.
    :return: mean quantity and unit of each ingredient-
    """

    # Most frequent measure
    m = np.max([len(l) for l in [ml, gr, garnish]])

    # All "nan" values
    if m == 0:
        measuresUnit = 'nan'
        measuresMean = 'nan'
        return measuresUnit, measuresMean

    # If measured in "ml", "gr" or is a used as garnish
    if len(ml) == m:
        measuresUnit = "ml"
        measuresMean = np.mean(ml)
    elif len(gr) == m:
        measuresUnit = "gr"
        measuresMean = np.mean(gr)
    else:
        measuresUnit = st.mode(garnish)[0][0]
        measuresMean = 1

    return measuresUnit, measuresMean

def save_ingredients_information(cocktails):
    """
    Create ingredients table with relevant information using the "cocktails" dataset by grouping the information of
    the recipes in which the ingredients appear.

    :param cocktails: dataframe with the information of the ingredients of the cocktails as a different row.
    """

    # Group information by ingredient
    dataset = []
    for cocktail, data in cocktails.groupby("strIngredients"):

        # Ingredient name
        ingredients = data.strIngredients.values[0]
        ingredients = ingredients.replace('\'', "")

        # Tastes (remove "nans")
        Tastes = np.array(data.Basic_taste.values)
        tastes = []
        for t in Tastes:
            if isinstance(t, float):
                if not math.isnan(t): tastes.append(t)
            else: tastes.append(t)
        tastes = tastes[0] if len(tastes) != 0 else 'nan'

        # Type of alcohol (remove "nans")
        AlcType = np.array(data.Alc_type.values)
        alcType = []
        for t in AlcType:
            if isinstance(t, float):
                if not math.isnan(t): alcType.append(t)
            else: alcType.append(t)
        alcType = alcType[0] if len(alcType) != 0 else 'nan'

        # Measures (remove "nans")
        ValuesML = data.Value_ml.values
        valuesML = [t for t in ValuesML if not math.isnan(t)]
        ValuesGR = data.Value_gr.values
        valuesGR = [t for t in ValuesGR if not math.isnan(t)]

        ValuesGarnish = data.Garnish_type.values
        valuesGarnish = []
        for t in ValuesGarnish:
            if isinstance(t, float):
                if not math.isnan(t): valuesGarnish.append(t)
            else: valuesGarnish.append(t)

        # Measures of each ingredient
        measuresUnit, measuresMean = info_measures(valuesML, valuesGR, valuesGarnish)

        # Ingredient information
        row = {"ingredients": ingredients,
               "alcTypes": alcType,
               "tastes": tastes,
               "measuresUnit": measuresUnit,
               "measuresMean": measuresMean}

        dataset.append(row)

    # Dataframe with all the ingredients information
    dataset = pd.DataFrame(dataset)
    dataset.to_csv("ingredientsInformation.csv", index=False)


# Read original dataset
cocktails = pd.read_csv("cocktails.csv")

# Ingredients information
save_ingredients_information(cocktails)


import pandas as pd
import numpy as np
import math
from nltk import tokenize

SMOOTH_FACTOR = 0.1

def listToStr(l, number=False):
    comma = "" if number else "\'"
    return "["+",".join([comma+str(v).replace('\'',"")+comma for v in l])+"]"

def merge_measures(ml, gr, garnishValue, garnishType):
    """
    Merge all the measures information to have a compact representation of the information.

    :param ml: milliliters of each ingredient of a recipe.
    :param gr: grams of each ingredient of a recipe.
    :param garnishValue: amount of garnish of each ingredient of a recipe.
    :param garnishType: type of garnish of each ingredient of a recipe.
    :return: measures (original and normalized) and units of the measures for all the ingredients of a recipe.
    """
    
    # Quantities
    idx_ml = np.where(~np.isnan(ml))[0]
    idx_gr = np.where(~np.isnan(gr))[0]
    
    # Measures
    measures = garnishValue.copy()
    measuresUnit = garnishType.copy()
    for i in idx_ml:
        measures[i] = int(ml[i])
        measuresUnit[i] = "ml"
    for i in idx_gr:
        measures[i] = int(gr[i])
        measuresUnit[i] = "gr"
    measures = [(float(m) if "/" not in m else eval(m)) if isinstance(m, str) else (0 if math.isnan(m) else float(m)) for m in measures]

    # Normalize measures
    normalizedMeasures = measures.copy()
    normalizedMeasures = [m + SMOOTH_FACTOR for m in normalizedMeasures]
    suma = np.sum(normalizedMeasures)

    # Smoothing factor so that no 0 values
    normalizedMeasures = np.array(normalizedMeasures) / suma if suma else np.zeros(len(normalizedMeasures))
    
    return measures, normalizedMeasures, measuresUnit

def Case_Library(cocktails):
    """
    Create Case Library using the "cocktails" dataset by grouping the information of the ingredients of the recipes.

    :param cocktails: dataframe with the information of the ingredients of the cocktails as a different row.
    """

    # Group information by recipe
    dataset = []
    for cocktail, data in cocktails.groupby("strDrink"):

        # Unique values for the recipe
        category = data.strCategory.iloc[0]
        glass = data.strGlass.iloc[0]
        instructions = data.strInstructions.iloc[0]

        # Information of each ingredient of the recipe (ingredient name, type of alcohol and taste)
        ingredients = listToStr(data.strIngredients.values)
        ingredientsTypes = listToStr(data.Alc_type.values)
        tastes = listToStr(data.Basic_taste.values)

        # Measures of each ingredient
        measuresText = data.strMeasures.values
        valuesML = data.Value_ml.values
        valuesGR = data.Value_gr.values
        valuesGarnish = data.Garnish_amount.values
        typesGarnish = data.Garnish_type.values
        measuresMerge, measuresNorm, measuresUnit = merge_measures(valuesML, valuesGR, valuesGarnish, typesGarnish)

        # Convert lists to string
        measuresMerge = listToStr(measuresMerge, number=True)
        measuresNorm = listToStr(measuresNorm, number=True)
        measuresUnit = listToStr(measuresUnit, number=False)
        measuresText = listToStr(measuresText, number=False)

        # Instructions "pre-processing": tokenization and capitalize
        instructions = instructions.replace("oz.", "oz")
        instructions = tokenize.sent_tokenize(instructions)
        instructions = [sent.capitalize() for sent in instructions]

        # Case definition: cocktail
        row = {"cocktailName": cocktail,
               "categories": category,
               "glass": glass,
               "ingredients": ingredients,
               "alcTypes": ingredientsTypes,
               "tastes": tastes,
               "measures": measuresMerge,
               "measuresUnit": measuresUnit,
               "measuresNorm": measuresNorm,
               "measuresText": measuresText,
               "instructions": instructions,
               "utility": 0.5,
               "retrievedSuccess": 0,
               "usedSuccess": 0,
               "retrievedFailure": 0,
               "usedFailure": 0}
        dataset.append(row)

    # Case Library with all the available cases
    dataset = pd.DataFrame(dataset)
    dataset.to_csv("cocktailsGrouped.csv", index=False)

    # Deleted cocktails
    dataset.iloc[0:0].to_csv("cocktailsGroupedDeleted.csv", index=False)


# Read original dataset
cocktails = pd.read_csv("cocktails.csv")

# Create Case Library
Case_Library(cocktails)

# Then use this to read it
def read_csv_with_lists(file_path, columns):
    converters = {column: eval for column in columns}
    return pd.read_csv(file_path, converters=converters)
df = read_csv_with_lists('cocktailsGrouped.csv', 
                         ["ingredients", "alcTypes", "tastes", "measures", "measuresNorm",
                          "valuesML", "valuesGR", "valuesGarnish", "typesGarnish"])
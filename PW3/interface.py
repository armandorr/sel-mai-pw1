import ast
import time
import copy
import traceback
import numpy as np
import pandas as pd
import streamlit as st

import openai
from PIL import Image

from src.retrieve import retrieve
from src.reuse import reuse
from src.revise import revise
from src.retain import retain, forget
openai.api_key = st.secrets["API_KEY"]
path_to_root_folder = './'

# Wheter to use GPT-3 or not for the instructions post-processing
USE_GPT = False

#########################################################################################
######################################## LOGICS #########################################
#########################################################################################

def number_instructions(originalCase):
    """
    Number the instructions of a case

    :param originalCase: the original case
    :return: the case with numbered instructions
    """

    newCase = originalCase.copy()
    newInstructions = [f"{str(num + 1)}\. {sent.capitalize()}" for num, sent in enumerate(originalCase['instructions'])]
    newCase['instructions'] = newInstructions
    return newCase


def showCase(stComponent, case, show=True, addNums=True):
    """
    Show a case in the web interface

    :param stComponent: the streamlit component
    :param case: the case to show
    :param show: whether to show the case or not
    :param addNums: whether to add numbers to the instructions or not
    :return: the ingredients and instructions of the case
    """

    caseToShow = copy.deepcopy(case)
    endl = '\n'
    endlApp = '  \n  '
    if show:
        stComponent.markdown(f'{caseToShow.cocktailName}')
        stComponent.markdown(f'A {caseToShow.categories.lower()} in a {caseToShow.glass.lower()}')
    ingredientsCompleted = []
    measures = eval(caseToShow.measures) if isinstance(caseToShow.measures,str) else caseToShow.measures
    for t,m,u,i in zip(caseToShow.measuresText, measures, caseToShow.measuresUnit, caseToShow.ingredients):
        measure = int(m) if int(m) != 0 else -1
        if measure == -1:
            measure = "" if t == "nan" else t.capitalize()
        else:
            measure = str(measure) + (u if u != 'nan' else '') + " of"
        
        ingredientsCompleted.append(f"{measure} {i}")

    ing = endl.join(ingredientsCompleted)
    ingredientsCompleted.insert(0, '**Ingredients**')
    
    if show:
        stComponent.markdown(f'{endlApp.join(ingredientsCompleted)}')

    if addNums:
        caseToShow = number_instructions(caseToShow)

    instructionsCompleted = caseToShow.instructions.copy()
    ins = endl.join(instructionsCompleted)
    instructionsCompleted.insert(0, '**Instructions**')
    if show:
        stComponent.markdown(f'{endlApp.join(instructionsCompleted)}')

    return ing, ins

@st.cache_data
def computeInstructions(ing, ins):
    """
    Post-process the instructions of a case using GPT-3

    :param ing: the ingredients of the case
    :param ins: the instructions of the case
    :return: the post-processed instructions
    """

    question = f"""
Having these ingredients:

{ing}

And these instructions: 

{ins}

I'm a bartender and I need you to generate a new set of adapted instructions modifying the original instruction to adapt them to the ingredients list. Make sure not not give instructions with ingredients that don't appear on that list. Provide only the instructions:
    """

    response = openai.ChatCompletion.create(
    model="gpt-3.5-turbo",
    messages=[
            {"role": "system", "content": "You are a helpful assistant."},
            {"role": "user", "content": question},
        ]
    )
    response = response['choices'][0]['message']['content']
    return [ins.replace('.', '\\.') for ins in response.split("\n")] if response.startswith("1") else None

def read_csv_with_lists(file_path, columns):
    converters = {column: eval for column in columns}
    return pd.read_csv(file_path, converters=converters)

cocktails = read_csv_with_lists(path_to_root_folder+'data/cocktailsGrouped.csv', 
                                ["ingredients", "alcTypes", "tastes", "measures", "measuresNorm",
                                 "measuresUnit", "measuresText", "valuesML", "valuesGR", "valuesGarnish", "typesGarnish", "instructions"])

cocktailsDeleted = read_csv_with_lists(path_to_root_folder+'data/cocktailsGroupedDeleted.csv', 
                                ["ingredients", "alcTypes", "tastes", "measures", "measuresNorm",
                                 "measuresUnit", "measuresText", "valuesML", "valuesGR", "valuesGarnish", "typesGarnish", "instructions"])

allIngredients = set(np.unique(np.concatenate(cocktails.ingredients.values)))
allCategories = set(np.unique(cocktails.categories.values))
allTastes = set(np.unique(np.concatenate(cocktails.tastes.values)))
allTastes.remove("nan")
allTypes = set(np.unique(np.concatenate(cocktails.alcTypes.values)))
allTypes.remove("nan")

ingredientsSelected = set()
categoriesSelected = set()
tastesSelected = set()
typesSelected = set()
forbiddenIngredients = set()
forbiddenCategories = set()
forbiddenTastes = set()
forbiddenTypes = set()


#########################################################################################
####################################### INTERFACE #######################################
#########################################################################################

try:
    st.title("Cocktails CBR")
    image = Image.open(path_to_root_folder+'images/cocktails.jpg')
    st.image(image, caption='Developed by Sonia Rabanaque, Armando Rodriguez, Pablo Ruiz and Hasnain Shafqat')

    with st.form("Cocktail creation"):

        first, second = st.columns([9,2])
        first.header("Cocktail creation")
        USE_GPT = second.checkbox('Use GPT')
        
        cocktailName = st.text_input(label="Indicate your cocktail name", 
                                        placeholder ="Insert the name of our cocktail",)

        st.markdown('#### Desired options:')

        first, second = st.columns(2)
        ingredientsSelected = set(first.multiselect('Select the ingredients that you want',
                                                    sorted(set(allIngredients)-forbiddenIngredients), []))
        categoriesSelected = set(second.multiselect('Select the categories that you want your cocktail to be',
                                                    sorted(set(allCategories)-forbiddenCategories), []))
        
        first, second = st.columns(2)
        tastesSelected = set(first.multiselect('Select the basic tastes that you want',
                                            sorted(set(allTastes)-forbiddenTastes), []))
        typesSelected = set(second.multiselect('Select the types of alcohol that you want',
                                            sorted(set(allTypes)-forbiddenTypes), []))

        
        st.markdown('#### Forbidden options:')
        first, second = st.columns(2)
        forbiddenIngredients = set(first.multiselect("Select the ingredients that you don't want",
                                                    sorted(set(allIngredients)-ingredientsSelected), []))
        forbiddenCategories = set(second.multiselect("Select the categories that you don't want",
                                                    sorted(set(allCategories)-categoriesSelected), []))

        first, second = st.columns(2)
        forbiddenTastes = set(first.multiselect("Select the basic tastes that you don't want",
                                                sorted(set(allTastes)-tastesSelected), []))
        forbiddenTypes = set(second.multiselect("Select the types of alcohol that you don't want",
                                                sorted(set(allTypes)-typesSelected), []))

        cols = st.columns(3)
        create_cocktail_button = cols[-1].form_submit_button('Create the cocktail')
        

    if create_cocktail_button or 'create_cocktail_button_clicked' in st.session_state:
        st.session_state['create_cocktail_button_clicked'] = True
        st.session_state.pop("evaluatedCase", None)
        st.session_state.pop("alreadyRetained", None)

        query = {"ingredients": ingredientsSelected,
                "alcTypes": typesSelected,
                "tastes": tastesSelected,
                "categories": categoriesSelected}
        excludedQuery = {"ingredients_excluded": forbiddenIngredients,
                        "alcTypes_excluded": forbiddenTypes,
                        "tastes_excluded": forbiddenTastes,
                        "categories_excluded": forbiddenCategories}

        
        #########################################################################################
        ####################################### RETRIEVE ########################################
        #########################################################################################
        if create_cocktail_button or "retrievedCases" not in st.session_state:
            print("Retrieving...", end=" ")
            retrievedCases = retrieve(query, cocktails, excludedQuery, n=10)
            st.session_state['retrievedCases'] = retrievedCases
            print("Done")
        
        retrievedCases = st.session_state['retrievedCases']
        retrievedCase = retrievedCases.iloc[0]
        first, second = st.columns(2)
        first.markdown("#### Retrieved cocktail")
        showCase(first, retrievedCase)

        #########################################################################################
        ######################################### REUSE #########################################
        #########################################################################################
        if create_cocktail_button or "adaptedCase" not in st.session_state:
            print("Adapting...", end=" ")
            adaptedCase = reuse(retrievedCases, query, excludedQuery)
        
            newIns = None
            if USE_GPT:
                ing, ins = showCase(second, adaptedCase, show=False)
                newIns = computeInstructions(ing, ins)
                if newIns:
                    adaptedCase.instructions = newIns
            if cocktailName != "":
                adaptedCase.cocktailName = cocktailName
            
            st.session_state['adaptedCase'] = adaptedCase
            print("Done")
        
        adaptedCase = st.session_state['adaptedCase']
        second.markdown("#### Adapted cocktail")
        ing, ins = showCase(second, adaptedCase, addNums=not USE_GPT)

        #########################################################################################
        ######################################## REVISE #########################################
        #########################################################################################
        # with st.form("Cocktail evaluation"):
        st.header("Cocktail evaluation")
        evaluation = int(st.slider('Please evaluate the cocktail', 0, 10, 5))
        cols = st.columns(4)
        evaluate_cocktail_button = cols[-1].button('Evaluate')
        if evaluate_cocktail_button:
            print("Revising...", end=" ")
            evaluatedCase, reason = revise(cocktails, cocktailsDeleted,
                                           adaptedCase, excludedQuery,
                                           retrievedCases, evaluation)
            if evaluatedCase is None:
                st.session_state.pop("retrievedCases", None)
                st.session_state.pop("adaptedCase", None)
                st.session_state.pop("evaluatedCase", None)
                st.session_state["GoingBack"] = True
                st.session_state["reason"] = reason
                forget(cocktails)
                st.experimental_rerun()
            else:
                st.session_state['evaluatedCase'] = evaluatedCase
                st.session_state["GoingBack"] = False
                st.session_state.pop("reason", None)
                print("Done")

        if "GoingBack" in st.session_state and st.session_state["GoingBack"]:
            st.warning(f"Obtaining another case because the {st.session_state['reason']}")
        
        #########################################################################################
        ######################################## RETAIN #########################################
        #########################################################################################
        if 'evaluatedCase' in st.session_state:
            if 'alreadyRetained' in st.session_state:
                saved, distance, deletedNum = st.session_state['alreadyRetained']
            else:
                print("Retaining...", end=" ")
                saved, distance, deletedNum = retain(evaluatedCase, retrievedCase, cocktails, cocktailsDeleted)
                st.session_state['alreadyRetained'] = saved, distance, deletedNum
                print("Done")
            if saved:
                st.success('The adapted case has been evaluated correctly and has been accepted into our CBR system!', icon="✅")
                if evaluation >= 9.5:
                    st.balloons()
            else:
                st.error(f'The adapted case has been evaluated correctly but has not been accepted into our CBR system because its distance with the retrieved case is {round(distance,4)}', icon="🚨")

        cols = st.columns(3)
        cols[0].metric("Cases in system", value=str(round(len(cocktails)))+" cases", delta_color="off")
        cols[1].metric("Deleted cases", value=str(round(len(cocktailsDeleted)))+" case" + ("s" if int(len(cocktailsDeleted)) != 1 else ""), delta_color="off")
        cols[2].metric("Useful cases", value=str(round(np.sum(cocktails.utility >= 0.5)))+" cases", delta_color="off")

except Exception as e:
    traceback.print_exc()
    print(e)
    st.error('Unexpected error. Try again. If the error persist, please contact the administrators. ', icon="🚨")

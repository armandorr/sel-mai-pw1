import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# Path to the dataset in CSV format
BASE_PATH = '../Data/cocktails.csv'


def single_line_barplot(x, y, x_label, y_label):
    """
    Plot a single line barplot with the given data.

    :param x: The x values of the bars.
    :param y: The y values of the bars.
    :param x_label: The label of the x axis.
    :param y_label: The label of the y axis.
    :return: The figure and the axis of the plot.
    """

    # Define the figure and the axis
    fig, ax = plt.subplots(figsize=(len(x)*1.5, 5))

    bars = ax.bar(x, y)

    # Remove the top and right spines
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    # Set the labels and the ticks
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.set_xticklabels(x, rotation=45, ha='right')

    # Add the values on top of the bars
    for bar in bars:
        height = bar.get_height()
        ax.text(bar.get_x() + bar.get_width() / 2, height, height,
                ha='center', va='bottom')

    return fig, ax


def multiple_line_barplot(x, y, x_label, y_label, n_rows):
    """
    Plot a multiple line barplot with the given data.

    :param x: The x values of the bars.
    :param y: The y values of the bars.
    :param x_label: The label of the x axis.
    :param y_label: The label of the y axis.
    :param n_rows: The number of rows of the plot.
    :return: The figure and the axis of the plot.
    """

    # Calculate the number of bars per row
    num_per_row = len(x) // n_rows
    
    # Check if the number of bars is exact
    exact = True
    if len(x) %  n_rows != 0:
        exact = False

    # Define the figure and the axis
    fig, axs = plt.subplots(n_rows, 1, figsize=(10, n_rows*5))
    plt.subplots_adjust(hspace = 0.5)  # Adjust the height space between plots
    y_lim = max(y) + 0.1*max(y)  # add a little extra space for the numbers above the bars

    # Plot the bars
    for i in range(n_rows):
        start = i*num_per_row
        end = (i+1)*num_per_row

        # If the number of bars is not exact, plot the remaining bars in the last row
        if not exact and i == n_rows-1:
            end = len(x)

        # Plot the bars
        axs[i].bar(x[start:end], y[start:end])
        axs[i].spines['top'].set_visible(False)
        axs[i].spines['right'].set_visible(False)

        # Set the labels and the ticks
        axs[i].set_ylabel(y_label)
        axs[i].set_xticklabels(x[start:end], rotation=45, ha='right')
        axs[i].set_ylim([0, y_lim])

        # Add the exact numbers on top of each bar
        for x_pos, y_pos in zip(x[start:end], y[start:end]):
            axs[i].text(x_pos, y_pos, str(round(y_pos, 2)), ha='center', va='bottom')

    # Set the x label only in the last row
    axs[n_rows-1].set_xlabel(x_label)
    
    return fig, axs


def number_drinks(verbose=True):
    """
    Obtain the number of drinks in the dataset.

    :param verbose: Whether to print the number of drinks or not.
    :return: The number of drinks in the dataset.
    """

    # Read the dataset
    df = pd.read_csv(BASE_PATH, sep=',')
    number_drinks = len(df['strDrink'].unique())

    # Print the number of drinks
    if verbose:
        print("Number of drinks: {}".format(len(df)))
    
    return number_drinks


def category_count(save_plot_path=None, verbose=True):
    """
    Obtain the number of drinks per category.

    :param save_plot_path: The path to save the plot.
    :param verbose: Whether to print the number of drinks per category or not.
    :return: The number of drinks per category.
    """

    # Read the dataset
    df = pd.read_csv(BASE_PATH, sep=',')
    categories = df['strCategory'].value_counts()

    # Plot the categories
    fig, ax = single_line_barplot(categories.index, categories.values, 'Category', 'Count')

    # If a path is defined, save the plot
    if save_plot_path is not None:
        plt.savefig(save_plot_path, bbox_inches='tight')    

    # Print the categories count and the plot
    if verbose:
        print("Categories count: ")
        for category, count in categories.items():
            print("\t{}: {}".format(category, count))
        
        plt.show()

    return categories


def glass_count(save_plot_path=None, verbose=True):
    """
    Obtain the number of drinks per glass.

    :param save_plot_path: The path to save the plot.
    :param verbose: Whether to print the number of drinks per glass or not.
    :return: The number of drinks per glass.
    """


    # Read the dataset
    df = pd.read_csv(BASE_PATH, sep=',')
    glasses = df['strGlass'].value_counts()

    # Plot the glasses
    fig, ax = multiple_line_barplot(glasses.index, glasses.values, 'Glass', 'Count', 5)

    # If a path is defined, save the plot
    if save_plot_path is not None:
        plt.savefig(save_plot_path, bbox_inches='tight')    

    # Print the glasses count and the plot
    if verbose:
        print("Glasses count: ")
        for glass, count in glasses.items():
            print("\t{}: {}".format(glass, count))
        
        plt.tight_layout()
        plt.show()


    return glasses


def ingridients_count(save_plot_path=None, verbose=True):
    """
    Obtain the count of each different ingridients in the dataset.

    :param save_plot_path: The path to save the plot.
    :param verbose: Whether to print the number of different ingridients or not.
    :return: The count of each different ingridients in the dataset.
    """

    # Read the dataset
    df = pd.read_csv(BASE_PATH, sep=',')
    ingridients = df['strIngredients'].value_counts()

    # Plot the ingridients
    fig, ax = multiple_line_barplot(ingridients.index, ingridients.values, 'Ingridient', 'Count', 19)

    # If a path is defined, save the plot
    if save_plot_path is not None:
        plt.savefig(save_plot_path, bbox_inches='tight')    

    # Print the ingridients count and the plot
    if verbose:
        print("Ingridients count: ")
        for ingridient, count in ingridients.items():
            print("\t{}: {}".format(ingridient, count))
        
        plt.tight_layout()
        plt.show()

    return ingridients


def alcohol_count(save_plot_path=None, verbose=True):
    """
    Obtain the count of each different alcohol in the dataset.

    :param save_plot_path: The path to save the plot.
    :param verbose: Whether to print the count of different alcohol or not.
    :return: The count of each different alcohol in the dataset.
    """ 

    # Read the dataset
    df = pd.read_csv(BASE_PATH, sep=',')
    alcohol = df['Alc_type'].value_counts()

    # Plot the alcohol
    fig, ax = multiple_line_barplot(alcohol.index, alcohol.values, 'Alcohol', 'Count', 2)

    # If a path is defined, save the plot
    if save_plot_path is not None:
        plt.savefig(save_plot_path, bbox_inches='tight')    

    # Print the alcohol count and the plot
    if verbose:
        print("Alcohol count: ")
        for alc, count in alcohol.items():
            print("\t{}: {}".format(alc, count))
        
        plt.tight_layout()
        plt.show()

    return alcohol  


def taste_count(save_plot_path=None, verbose=True):
    """
    Obtain the count of each different taste in the dataset.

    :param save_plot_path: The path to save the plot.
    :param verbose: Whether to print the count of different taste or not.
    :return: The count of each different taste in the dataset.
    """

    # Read the dataset
    df = pd.read_csv(BASE_PATH, sep=',')
    taste = df['Basic_taste'].value_counts()

    # Plot the taste
    fig, ax = single_line_barplot(taste.index, taste.values, 'Taste', 'Count')

    # If a path is defined, save the plot
    if save_plot_path is not None:
        plt.savefig(save_plot_path, bbox_inches='tight')    

    # Print the taste count and the plot
    if verbose:
        print("Taste count: ")
        for t, count in taste.items():
            print("\t{}: {}".format(t, count))
        
        plt.tight_layout()
        plt.show()

    return taste


def n_ingridients_count(save_plot_path=None, verbose=True):
    """
    Obtain the count of the number of ingridients for each coctail.

    :param save_plot_path: The path to save the plot.
    :param verbose: Whether to print the number of ingridients or not.
    :return: The count of the number of ingridients for each coctail.
    """

    # Read the dataset
    df = pd.read_csv(BASE_PATH, sep=',')

    # Get the number of ingridients for each coctail
    ingredients_count_by_cocatil = df.groupby('strDrink')['strIngredients'].count()
    ingredients_by_coctails = ingredients_count_by_cocatil.value_counts()

    # Plot the n_ingridients
    fig, ax = single_line_barplot([str(i) for i in ingredients_by_coctails.index], ingredients_by_coctails.values, 'Number of ingredients per cocktail', 'Count')

    # If a path is defined, save the plot
    if save_plot_path is not None:
        plt.savefig(save_plot_path, bbox_inches='tight')    

    # Print the n_ingridients count and the plot
    if verbose:
        print("Number of ingridients count: ")
        for n, count in ingredients_by_coctails.items():
            print("\t{}: {}".format(n, count))
        
        plt.tight_layout()
        plt.show()

    return ingredients_by_coctails


def cocktail_type(save_plot_path=None, verbose=True):
    """
    Obtain the count of each different cocktail type in the dataset.

    :param save_plot_path: The path to save the plot.
    :param verbose: Whether to print the count of different cocktail type or not.
    :return: The count of each different cocktail type in the dataset.
    """

    # Read the dataset
    df = pd.read_csv(BASE_PATH, sep=',')

    # Non alcoholic ingridients
    non_alc = df['Alc_type'].notnull() 
    n_drinks = number_drinks(verbose=False)

    # Group by cocktail and count the number of alcoholic ingredients
    alc_count = df.groupby('strDrink')['Alc_type'].count()

    # Filter for cocktails with 0 alcoholic ingredients
    non_alcoholic_cocktails = alc_count[alc_count == 0]
    alcoholic_cocktails = n_drinks - len(non_alcoholic_cocktails)

    x = ['Alcoholic', 'Non-alcoholic']
    y = [alcoholic_cocktails, len(non_alcoholic_cocktails)]

    # Plot the alcohol
    fig, ax = single_line_barplot(x, y, 'Cocktail type', 'Count')

    if save_plot_path is not None:
        plt.savefig(save_plot_path, bbox_inches='tight')

    if verbose:
        # Check if there are any non-alcoholic cocktails
        if non_alcoholic_cocktails.empty:
            print("There are no non-alcoholic cocktails.")
        else:
            print("These are the non-alcoholic cocktails:")
            print(non_alcoholic_cocktails.index.values) 

        print("There are {} alcoholic cocktails.".format(alcoholic_cocktails))

        plt.show()

    return alcoholic_cocktails, non_alcoholic_cocktails


def ratio_alcohol(save_plot_path=None, verbose=True):
    """
    Obtain the ratio of cocktails that include each alcohol type.

    :param save_plot_path: The path to save the plot.
    :param verbose: Whether to print the ratio of cocktails that include each alcohol type or not.
    :return: The ratio of cocktails that include each alcohol type.
    """

    # Read the dataset
    df = pd.read_csv(BASE_PATH, sep=',')

    # Total number of unique cocktails in the dataframe
    total_cocktails = df['strDrink'].nunique()

    # Count of unique cocktails each alcohol type appears in
    alc_type_counts = df.groupby('Alc_type')['strDrink'].nunique()

    # Ratio of cocktails that include each alcohol type
    alc_type_ratio = alc_type_counts / total_cocktails
    alc_type_ratio = alc_type_ratio.sort_values(ascending=False)
    alc_type_ratio = alc_type_ratio.round(4)

    # Plot the alcohol
    fig, ax = multiple_line_barplot(alc_type_ratio.index, alc_type_ratio.values, 'Alcohol', 'Ratio of cocktails that include each alcohol type', 2)

    # If a path is defined, save the plot
    if save_plot_path is not None:
        plt.savefig(save_plot_path, bbox_inches='tight')

    # Print the alcohol count and the plot
    if verbose:
        print("Alcohol ratio: ")
        for alc, ratio in alc_type_ratio.items():
            print("\t{}: {}".format(alc, ratio))
        
        plt.tight_layout()
        plt.show()

    return alc_type_ratio

def ratio_taste(save_plot_path=None, verbose=True):
    """
    Obtain the ratio of cocktails that include each taste.

    :param save_plot_path: The path to save the plot.
    :param verbose: Whether to print the ratio of cocktails that include each taste or not.
    :return: The ratio of cocktails that include each taste.
    """

    # Read the dataset
    df = pd.read_csv(BASE_PATH, sep=',')

    # Total number of unique cocktails in the dataframe
    total_cocktails = df['strDrink'].nunique()

    # Count of unique cocktails each alcohol type appears in
    taste_counts = df.groupby('Basic_taste')['strDrink'].nunique()

    # Ratio of cocktails that include each alcohol type
    taste_ratio = taste_counts / total_cocktails
    taste_ratio = taste_ratio.sort_values(ascending=False)
    taste_ratio = taste_ratio.round(4)

    # Plot the alcohol
    fig, ax = single_line_barplot(taste_ratio.index, taste_ratio.values, 'Taste', 'Ratio of cocktails that include each taste')

    # If a path is defined, save the plot
    if save_plot_path is not None:
        plt.savefig(save_plot_path, bbox_inches='tight')

    # Print the alcohol count and the plot
    if verbose:
        print("Taste ratio: ")
        for t, ratio in taste_ratio.items():
            print("\t{}: {}".format(t, ratio))
        
        plt.show()
    
    return taste_ratio

def coocurrence_ingridient_ingridient(min_frequency, save_plot_path=None, verbose=True):
    """
    Obtain the co-occurrence matrix of ingredients.

    :param min_frequency: The minimum frequency of an ingredient to be considered.
    :param save_plot_path: The path to save the plot.
    :param verbose: Whether to plot the co-occurrence matrix of ingredients or not.
    :return: The co-occurrence matrix of ingredients.
    """

    # Read the dataset
    df = pd.read_csv(BASE_PATH, sep=',')

    # Create a binary matrix where 1 means the ingredient is in the cocktail and 0 otherwise
    binary_matrix = df.pivot_table(index='strDrink', columns='strIngredients', aggfunc='size', fill_value=0)

    # Ensure all values greater than 1 are set to 1
    binary_matrix = binary_matrix.applymap(lambda x: 1 if x > 0 else 0)

    # Calculate the co-occurrence matrix
    co_occurrence_matrix = np.dot(binary_matrix.transpose(), binary_matrix)

    # Set the diagonal to 0 since we're not interested in how many times an ingredient occurs with itself
    np.fill_diagonal(co_occurrence_matrix, 0)

    co_occurrence_df = pd.DataFrame(co_occurrence_matrix, columns=binary_matrix.columns, index=binary_matrix.columns)

    # You might want to filter the dataframe to only include ingredients that occur frequently
    filtered_co_occurrence_df = co_occurrence_df.loc[co_occurrence_df.sum(axis=1) > min_frequency, co_occurrence_df.sum(axis=0) > min_frequency]

    # Plot the co-occurrence matrix
    plt.figure(figsize=(10,8))
    sns.heatmap(filtered_co_occurrence_df, cmap='YlGnBu')

    plt.title('Co-Occurrence of Ingredients in Cocktails')
    plt.xlabel('Ingredients')
    plt.ylabel('Ingredients')

    if save_plot_path is not None:
        plt.savefig(save_plot_path, bbox_inches='tight')

    if verbose:
        plt.show()

    return filtered_co_occurrence_df


def coocurrence_alcohol_alcohol(min_frequency, save_plot_path=None, verbose=True):
    """
    Obtain the co-occurrence matrix of alcohol types.

    :param min_frequency: The minimum frequency of an alcohol type to be considered.
    :param save_plot_path: The path to save the plot.
    :param verbose: Whether to plot the co-occurrence matrix of alcohol types or not.
    :return: The co-occurrence matrix of alcohol types.
    """

    # Read the dataset
    df = pd.read_csv(BASE_PATH, sep=',')

    # Create a binary matrix where 1 means the ingredient is in the cocktail and 0 otherwise
    binary_matrix = df.pivot_table(index='strDrink', columns='Alc_type', aggfunc='size', fill_value=0)

    # Ensure all values greater than 1 are set to 1
    binary_matrix = binary_matrix.applymap(lambda x: 1 if x > 0 else 0)

    # Calculate the co-occurrence matrix
    co_occurrence_matrix = np.dot(binary_matrix.transpose(), binary_matrix)

    # Set the diagonal to 0 since we're not interested in how many times an ingredient occurs with itself
    np.fill_diagonal(co_occurrence_matrix, 0)

    co_occurrence_df = pd.DataFrame(co_occurrence_matrix, columns=binary_matrix.columns, index=binary_matrix.columns)

    # You might want to filter the dataframe to only include ingredients that occur frequently
    filtered_co_occurrence_df = co_occurrence_df.loc[co_occurrence_df.sum(axis=1) > min_frequency, co_occurrence_df.sum(axis=0) > min_frequency]

    # Plot the co-occurrence matrix
    plt.figure(figsize=(10,8))
    sns.heatmap(filtered_co_occurrence_df, cmap='YlGnBu')

    plt.title('Co-Occurrence of Alcohol types in Cocktails')
    plt.xlabel('Alcohol types')
    plt.ylabel('Alcohol types')

    if save_plot_path is not None:
        plt.savefig(save_plot_path, bbox_inches='tight')

    if verbose:
        plt.show()

    return filtered_co_occurrence_df


def coocurrence_taste_ingridients(min_frequency, save_plot_path=None, verbose=True):
    """
    Obtain the co-occurrence matrix of tastes and ingredients.

    :param min_frequency: The minimum frequency of a taste or ingridient to be considered.
    :param save_plot_path: The path to save the plot.
    :param verbose: Whether to plot the co-occurrence matrix of tastes and ingredients or not.
    :return: The co-occurrence matrix of tastes and ingredients.
    """

    # Read the dataset
    df = pd.read_csv(BASE_PATH, sep=',')

    # Create a new DataFrame with a list of ingredients for each cocktail
    df_ingredients = df.groupby(['strDrink', 'Basic_taste'])['strIngredients'].apply(list).reset_index()

    # Drop rows with NaN taste
    df_ingredients = df_ingredients.dropna(subset=['Basic_taste'])

    import numpy as np

    # Get a list of all unique ingredients and tastes
    ingredients = df['strIngredients'].unique()
    tastes = df['Basic_taste'].dropna().unique()

    # Initialize a co-occurrence matrix of zeros
    co_occurrence_matrix = np.zeros((len(ingredients), len(tastes)))

    # Create an index for ingredients and tastes
    ingredient_index = {ingredient: i for i, ingredient in enumerate(ingredients)}
    taste_index = {taste: i for i, taste in enumerate(tastes)}

    # Fill the co-occurrence matrix
    for _, row in df_ingredients.iterrows():
        taste = row['Basic_taste']
        for ingredient in row['strIngredients']:
            i = ingredient_index[ingredient]
            j = taste_index[taste]
            co_occurrence_matrix[i, j] += 1

    # Convert the co-occurrence matrix to a DataFrame
    co_occurrence_df = pd.DataFrame(co_occurrence_matrix, index=ingredients, columns=tastes)
    filtered_co_occurrence_df = co_occurrence_df.loc[co_occurrence_df.sum(axis=1) > min_frequency, co_occurrence_df.sum(axis=0) > min_frequency]

    # Plot the co-occurrence matrix
    plt.figure(figsize=(10,8))
    sns.heatmap(filtered_co_occurrence_df, cmap='YlGnBu')

    plt.title('Co-Occurrence of Tastes and Ingredients in Cocktails')
    plt.xlabel('Tastes')
    plt.ylabel('Ingredients')

    if save_plot_path is not None:
        plt.savefig(save_plot_path, bbox_inches='tight')

    if verbose:
        plt.show()

    return filtered_co_occurrence_df


def coocurrence_alcohol_glass(min_frequency, save_plot_path=None, verbose=True):
    """
    Obtain the co-occurrence matrix of alcohol types and glasses.

    :param min_frequency: The minimum frequency of an alcohol type or glass to be considered.
    :param save_plot_path: The path to save the plot.
    :param verbose: Whether to plot the co-occurrence matrix of alcohol types and glasses or not.
    :return: The co-occurrence matrix of alcohol types and glasses.
    """

    # Read the dataset
    df = pd.read_csv(BASE_PATH, sep=',')

    # Create a new DataFrame with a list of ingredients for each cocktail
    df_ingredients = df.groupby(['strDrink', 'Alc_type'])['strGlass'].apply(list).reset_index()

    # Drop rows with NaN taste
    df_ingredients = df_ingredients.dropna(subset=['Alc_type'])

    # Get a list of all unique ingredients and tastes
    ingredients = df['strGlass'].unique()
    tastes = df['Alc_type'].dropna().unique()

    # Initialize a co-occurrence matrix of zeros
    co_occurrence_matrix = np.zeros((len(ingredients), len(tastes)))

    # Create an index for ingredients and tastes
    ingredient_index = {ingredient: i for i, ingredient in enumerate(ingredients)}
    taste_index = {taste: i for i, taste in enumerate(tastes)}

    # Fill the co-occurrence matrix
    for _, row in df_ingredients.iterrows():
        taste = row['Alc_type']
        for ingredient in row['strGlass']:
            i = ingredient_index[ingredient]
            j = taste_index[taste]
            co_occurrence_matrix[i, j] += 1

    # Convert the co-occurrence matrix to a DataFrame
    co_occurrence_df = pd.DataFrame(co_occurrence_matrix, index=ingredients, columns=tastes)

    # You might want to filter the dataframe to only include ingredients that occur frequently
    filtered_co_occurrence_df = co_occurrence_df.loc[co_occurrence_df.sum(axis=1) > min_frequency, co_occurrence_df.sum(axis=0) > min_frequency]

    # Plot the co-occurrence matrix
    plt.figure(figsize=(10,8))
    sns.heatmap(filtered_co_occurrence_df, cmap='YlGnBu')

    plt.title('Co-Occurrence of Glasses and Alcohol Types in Cocktails')
    plt.ylabel('Glass')
    plt.xlabel('Alcohol Type')

    if save_plot_path is not None:
        plt.savefig(save_plot_path, bbox_inches='tight')

    if verbose:
        plt.show()

    return filtered_co_occurrence_df



def coocurrence_glass_taste(min_frequency, save_plot_path=None, verbose=True):
    """
    Obtain the co-occurrence matrix of glasses and tastes.

    :param min_frequency: The minimum frequency of a glass or taste to be considered.
    :param save_plot_path: The path to save the plot.
    :param verbose: Whether to plot the co-occurrence matrix of glasses and tastes or not.
    :return: The co-occurrence matrix of glasses and tastes.
    """

    # Read the dataset
    df = pd.read_csv(BASE_PATH, sep=',')

    # Create a new DataFrame with a list of ingredients for each cocktail
    df_ingredients = df.groupby(['strDrink', 'strGlass'])['Basic_taste'].apply(list).reset_index()

    # Drop rows with NaN taste
    df_ingredients = df_ingredients.dropna(subset=['Basic_taste'])

    # Get a list of all unique ingredients and tastes
    ingredients = df['strGlass'].unique()
    tastes = df['Basic_taste'].dropna().unique()

    # Initialize a co-occurrence matrix of zeros
    co_occurrence_matrix = np.zeros((len(ingredients), len(tastes)))

    # Create an index for ingredients and tastes
    ingredient_index = {ingredient: i for i, ingredient in enumerate(ingredients)}
    taste_index = {taste: i for i, taste in enumerate(tastes)}

    # Fill the co-occurrence matrix
    for _, row in df_ingredients.iterrows():
        taste = row['Basic_taste']
        for ingredient in row['strGlass']:
            i = ingredient_index[ingredient]
            j = taste_index[taste]
            co_occurrence_matrix[i, j] += 1

    # Convert the co-occurrence matrix to a DataFrame
    co_occurrence_df = pd.DataFrame(co_occurrence_matrix, index=ingredients, columns=tastes)

    # You might want to filter the dataframe to only include ingredients that occur frequently
    filtered_co_occurrence_df = co_occurrence_df.loc[co_occurrence_df.sum(axis=1) > min_frequency, co_occurrence_df.sum(axis=0) > min_frequency]

    # Plot the co-occurrence matrix
    plt.figure(figsize=(10,8))
    sns.heatmap(filtered_co_occurrence_df, cmap='YlGnBu')

    plt.title('Co-Occurrence of Glasses and Tastes in Cocktails')
    plt.ylabel('Glass')
    plt.xlabel('Taste')

    if save_plot_path is not None:
        plt.savefig(save_plot_path, bbox_inches='tight')

    if verbose:
        plt.show()

    return filtered_co_occurrence_df

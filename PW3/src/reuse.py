import pandas as pd
from typing import Set
import numpy as np
import re
import math

def read_csv_with_lists(file_path, columns):
    converters = {column: eval for column in columns}
    return pd.read_csv(file_path, converters=converters)

ingr_path = './data/ingredientsInformation.csv'
ingredientsType = read_csv_with_lists(ingr_path, [])

#########################################################################################
######################################## GENERAL ########################################
#########################################################################################

SMOOTH_FACTOR = 0.1
columns = ['ingredients', 'alcTypes', 'tastes', 'measuresUnit', 'measures', 'measuresText']     # MAINTAIN ORDER!!!

def index_value(value, col, case, all=False):
    """
    Get the index in which there is a certain value.

    :param value: value to search.
    :param col: attribute or column where the value is present.
    :param case: case where the value has to be searched.
    :param all: boolean indicating if all the indices have to be returned or a single one (random)
    :return: index of list of indices (if all=True) where the value is present in the case for a certain column.
    """

    # Indices where condition is fulfilled
    valuesCol = np.array(case[col])
    idxs = np.where(valuesCol == value)[0]

    # Return all indices or one of them
    if all: return idxs
    return np.random.choice(idxs)

def normalize_measures(originalCase, totalMeasures):
    """
    Normalize the measures to obtain the proportion of each ingredient in the cocktail and modify the
    measures such that the total quantity of the new case is the same as the original one.

    :param originalCase: current recipe of the cocktail.
    :param totalMeasures: total quantity of the retrieved case.
    :return: modified current recipe according to the measures attributes.
    """

    newCase = originalCase.copy()

    # Measures (smoothing)
    measures = originalCase['measures']
    measures = [0 if math.isnan(m) else m for m in measures]
    normalizedMeasures = [m + SMOOTH_FACTOR for m in measures]
    suma = np.sum(normalizedMeasures)

    # Suma will never be 0 for the smoothing factor
    normalizedMeasures = np.array(normalizedMeasures) / suma if suma else np.zeros(len(normalizedMeasures))

    # New measures
    measures = np.round(normalizedMeasures * totalMeasures)
    newCase.loc['measuresNorm'] = "["+str(",".join([str(n) for n in normalizedMeasures]))+"]"
    newCase.loc['measures'] = "["+str(",".join([str(n) for n in measures]))+"]"

    return newCase

#########################################################################################
##################################### INSTRUCTIONS ######################################
#########################################################################################

def obtain_instructions_ingredients(instructions, ingredients):
    """
    Get the instructions that refer to a particular set of ingredients.

    :param instructions: instructions of the cocktail.
    :param ingredients: ingredients to focus on.
    :return: steps of the cocktail instructions where the ingredients are present.
    """

    # Instructions where the ingredients are present
    steps = []; idxSteps = []
    for i, step in enumerate(instructions):
        for ingredient in ingredients:
            if ingredient.lower() in step.lower():
                steps.append(step)
                idxSteps.append(i)

    return steps, idxSteps

def add_instructions(originalInstructions, newInstructions):
    """
    Add new instructions to the current cocktail recipe.

    :param originalInstructions: set of instructions of the current recipe.
    :param newInstructions: set of new instructions to be added.
    :return: new set of instructions of the recipe after adding the new ones.
    """

    instructions = originalInstructions.copy()

    # Add new instructions
    for instruction in newInstructions:
        if instruction not in instructions:
            instructions.append(instruction)

    return instructions

def remove_partial_step(originalStep, ingredientsRemove):
    """
    Remove some part of the step, the one referring to some instructions, but not the whole instruction.

    :param originalStep: step or instruction to be removed partially.
    :param ingredientsRemove: set of ingredients to remove from the step.
    :return: instruction after removing the part related to the corresponding ingredient.
    """

    step = str(originalStep)

    # Remove each ingredient (and quantities, if any)
    for ingredientRemove in ingredientsRemove:
        step = re.sub(r"(of)* \b{}\b".format(ingredientRemove.lower()), "", step)
        step = re.sub("\d/\d (of)*", "", step)
        step = re.sub("\d", "", step)

    return step

def remove_instructions(originalInstructions, oldInstructions, idxInstructions, allIngredients, ingredientsRemove):
    """
    Remove instructions related to some ingredients.

    :param originalInstructions: set of instructions of the current recipe.
    :param oldInstructions: set of instructions to be modified, the ones related to the set of ingredients to remove.
    :param idxInstructions: indices of the set of "oldInstructions" in the "instructions".
    :param allIngredients: set of ingredients of the current recipe.
    :param ingredientsRemove: set of ingredients to remove from the recipe and, therefore, from the instructions.
    :return: new set of instructions of the current recipe after eliminating the ones (complete or partial) related to
     the ingredients to remove.
    """

    instructions = originalInstructions.copy()

    # Ingredients to be kept
    remainingIngredients = set(allIngredients) - set(ingredientsRemove)

    # Modify steps related to the ingredients to remove
    removeSteps = []
    for i, step in zip(idxInstructions, oldInstructions):

        # If another ingredient in the step, remove only the corresponding ingredients, not the whole step
        if any([ingredient.lower() in step.lower() for ingredient in remainingIngredients]):
            instructions[i] = remove_partial_step(step, ingredientsRemove)

        # If no other ingredient, remove whole step (save index)
        else: removeSteps.append(i)

    # Remove complete instructions (if any)
    if len(removeSteps) > 0:
        instructions = list(np.delete(instructions, removeSteps))

    return instructions

def substitute_instructions(originalInstructions, ingredientNew, ingredientOld):
    """
    Modify the instructions by substituting one ingredient for another one.

    :param originalInstructions: set of instructions of the current recipe.
    :param ingredientNew: ingredient to introduce, the one to substitute.
    :param ingredientOld: ingredient to remove, the one to be substituted.
    :return: new set of instructions of the recipe after substituting one ingredient for another one.
    """

    instructions = originalInstructions.copy()

    # Substitute ingredient current case by the selected by the user
    for i, step in enumerate(instructions):
        if ingredientOld.lower() in step.lower():
            step = step.replace(ingredientOld.lower(), ingredientNew.lower())
            instructions[i] = step

    return instructions

#########################################################################################
###################################### INGREDIENTS ######################################
#########################################################################################

def obtain_information_ingredient(value, col, infoSupplier, columns):
    """
    Obtain the relevant information of an ingredient.

    :param value: value to consider (name of the ingredient, alcohol type or taste).
    :param col: attribute or column where the value is present.
    :param infoSupplier: object used to obtain the information of the value.
    :param columns: attributes or columns to get the information.
    :return: information attributes of the specified value.
    """

    # Index of the ingredient in the list of ingredients
    idx = index_value(value, col, infoSupplier)

    # Information of the ingredient
    ingredientInfo = []
    for col in columns:
        if col in infoSupplier:
            value = infoSupplier[col][idx]
            if isinstance(value, float) and math.isnan(value): value = 'nan'
            ingredientInfo.append(value)

    return ingredientInfo

def select_similar_ingredient(ingredientsSimilar, ingredientsSelected, allSelected, allForbidden):
    """
    Select a similar ingredient according to a certain feature taking into account the user preferences, that is,
    that the ingredient is not selected and, if possible, that is forbidden by the user.

    :param ingredientsSimilar: set of ingredients considered as similar.
    :param ingredientsSelected: set of ingredients selected according to the user preferences.
    :param allSelected: all the selected preferences of the user.
    :param allForbidden: all the forbidden preferences of the user.
    :return: similar ingredient not selected but, if possible, forbidden by the user. Otherwise, return None.
    """

    # Not selected by the user
    ingredientsSimilarNotSelected = [ingr for ingr in ingredientsSimilar if ingr not in ingredientsSelected]
    if len(ingredientsSimilarNotSelected) == 0: return None

    # Forbidden ingredients are the best candidates to be substituted
    ingredientsSimilarForbidden = [ingr for ingr in ingredientsSimilarNotSelected if ingr in allForbidden[0]]
    if len(ingredientsSimilarForbidden) > 0: return np.random.choice(ingredientsSimilarForbidden)

    return np.random.choice(ingredientsSimilarNotSelected)

def search_ingredients_same_type(ingredient, ingredientsCase, col, ingredientsSelected,
                                 allSelected, allForbidden):
    """
    Search for a similar ingredient according to an attribute of an ingredient.

    :param ingredient: ingredient to consider.
    :param ingredientsCase: set of ingredients of the current recipe.
    :param col: attribute or column to look for.
    :param ingredientsSelected: selected user preferences of the col attribute.
    :param allSelected: all selected user preferences (ingredients, types of alcohol and tastes).
    :param allForbidden: all forbidden user preferences (ingredients, types of alcohol and tastes).
    :return: similar ingredient according to an attribute of an ingredient.
    """

    # Type selected ingredient by the user
    typeIngr = ingredientsType[ingredientsType['ingredients'] == ingredient][col].values
    if len(typeIngr) == 0: return None

    # Type ingredients of the current case
    others = ingredientsType[ingredientsType['ingredients'].isin(ingredientsCase)][['ingredients', col]]
    ingredientsCase = others['ingredients'].values
    typeIngredientsCase = others[col].values

    # Ingredients of the current case that are of the same type as the selected ingredient by the user
    idxsSimilar = np.where(typeIngredientsCase == typeIngr[0])[0]
    ingrSimilar = ingredientsCase[idxsSimilar]

    # Ingredient similar, but also not selected by the user
    ingredient = select_similar_ingredient(ingrSimilar, ingredientsSelected, allSelected, allForbidden)

    return ingredient

def add_new_ingredient(originalCase, typeIngredient, infoSupplier, col='ingredients'):
    """
    Add new ingredient to the recipe of the cocktail.

    :param originalCase: current recipe of the cocktail.
    :param typeIngredient: type of ingredient to be added to the recipe.
    :param infoSupplier: object used to obtain the information of the value.
    :param col: attribute or column to look for the type of ingredient.
    :return: modified recipe after adding the new ingredient.
    """

    newCase = originalCase.copy()

    # Ingredient information (and instructions)
    ingredientInfo = obtain_information_ingredient(typeIngredient, col, infoSupplier, columns)

    # If ingredient added directly (not based on a retrieved case), complete missing information
    if len(ingredientInfo) < len(columns):
        measures = ingredientsType[ingredientsType['ingredients'] == ingredientInfo[0]]['measuresMean'].values[0]
        ingredientInfo.append(measures)
        ingredientInfo.append("nan")        # not measures text information
        assert len(ingredientInfo) == len(columns)

        newInstructions = ["Add "+typeIngredient+"."]
        idxNewInstructions = []

    else:
        newInstructions, idxNewInstructions = obtain_instructions_ingredients(infoSupplier['instructions'],
                                                                              [ingredientInfo[0]])

    # Instructions of the new ingredient
    newIntructions = add_instructions(originalCase['instructions'], newInstructions)

    # Add to the case
    for col, ingrInfo in zip(columns, ingredientInfo):
        new_value = newCase.loc[col].copy()
        new_value.append(ingrInfo)
        newCase[col] = new_value
    newCase.loc['instructions'] = newIntructions

    return newCase

def substitute_ingredient(originalCase, ingredientNew, ingredientOld):
    """
    Substitute an ingredient of the recipe with another.

    :param originalCase: current recipe of the cocktail.
    :param ingredientNew: ingredient to introduce, the one to substitute.
    :param ingredientOld: ingredient to remove, the one to be substituted.
    :return: modified recipe after substituting the ingredientOld by the ingredientNew.
    """

    newCase = originalCase.copy()

    # Substitute ingredient
    idx = originalCase['ingredients'].index(ingredientOld)
    newCase['ingredients'][idx] = ingredientNew

    # Change instructions
    newIntructions = substitute_instructions(originalCase['instructions'], ingredientNew, ingredientOld)
    newCase['instructions'] = newIntructions

    return newCase

def incorporate_new_type_ingredient(originalCase, typeIngredient, neighbours, col, allSelected, allForbidden):
    """
    Incorporate a new ingredient in the recipe, by adding it.

    :param originalCase: current recipe of the cocktail.
    :param typeIngredient: type of ingredient to be incorporated in the recipe.
    :param neighbours: retrieved similar cases to the user preferences.
    :param col: attribute or column to look for the type of ingredient.
    :param allSelected: all selected user preferences (ingredients, types of alcohol and tastes).
    :param allForbidden: all forbidden user preferences (ingredients, types of alcohol and tastes).
    :return: modified recipe after incorporating a new ingredient in the recipe, by addition.
    """

    newCase = originalCase.copy()

    ingredientPresent = False
    for idx, neighbour in neighbours.iterrows():

        # If type of ingredient in other case, add it based on neighbour usage
        if typeIngredient in neighbour[col]:
            ingredientPresent = True
            newCase = add_new_ingredient(newCase, typeIngredient, neighbour, col)
            break

    # If not in any of the retrieved cases, add  ingredient of this type directly
    if not ingredientPresent:
        ingredientsThisType = ingredientsType[ingredientsType[col] == typeIngredient]['ingredients'].values
        ingredient = select_similar_ingredient(ingredientsThisType, [], None, allForbidden)
        newCase = add_new_ingredient(newCase, ingredient, ingredientsType)

    return newCase

def incorporate_new_ingredient(originalCase, ingredient, neighbours, ingredientsSelected,
                               allSelected, allForbidden):
    """
    Incorporate a new ingredient in the recipe, by substitution or addition.

    :param originalCase: current recipe of the cocktail.
    :param ingredient: ingredient to be introduced in the recipe.
    :param neighbours: retrieved similar cases to the user preferences.
    :param ingredientsSelected: selected user preferences of the col attribute.
    :param allSelected: all selected user preferences (ingredients, types of alcohol and tastes).
    :param allForbidden: all forbidden user preferences (ingredients, types of alcohol and tastes).
    :return: modified recipe after incorporating a new ingredient in the recipe, by substitution or addition.
    """

    newCase = originalCase.copy()

    # Ingredients of the same alcohol type and taste (similar cases)
    ingredientsSelected.remove(ingredient)
    ingredientSameAlcType = search_ingredients_same_type(ingredient, originalCase['ingredients'], 'alcTypes',
                                                         ingredientsSelected, allSelected, allForbidden)
    ingredientSameTaste = search_ingredients_same_type(ingredient, originalCase['ingredients'], 'tastes',
                                                       ingredientsSelected, allSelected, allForbidden)

    # If similar ingredient in the current solution (and not selected by the user), substitute it
    if ingredientSameAlcType:
        newCase = substitute_ingredient(newCase, ingredient, ingredientSameAlcType)

    elif ingredientSameTaste:
        newCase = substitute_ingredient(newCase, ingredient, ingredientSameTaste)

    # Not similar ingredient, add it
    else:
        newCase = incorporate_new_type_ingredient(newCase, ingredient, neighbours, 'ingredients',
                                                  allSelected, allForbidden)

    return newCase

def remove_ingredients_from_case(originalCase, element, col):
    """
    Remove an ingredient from the recipe of the cocktail.

    :param originalCase: current recipe of the cocktail.
    :param element: element to be removed.
    :param col: attribute or column to look for the element.
    :return: modified recipe after removing the corresponding ingredient.
    """

    newCase = originalCase.copy()

    # Indices of the elements where condition is fulfilled
    idxs = index_value(element, col, originalCase, all=True)
    ingredientsRemove = np.array(originalCase['ingredients'])[idxs]

    # Remove instructions of the forbidden ingredient
    oldInstructions, idxOldInstructions = obtain_instructions_ingredients(newCase['instructions'], ingredientsRemove)
    newIntructions = remove_instructions(originalCase['instructions'], oldInstructions, idxOldInstructions,
                                         originalCase['ingredients'], ingredientsRemove)

    # Remove from the case
    for col in columns:
        newCase[col] = list(np.delete(originalCase[col], idxs))
    newCase['instructions'] = newIntructions

    return newCase

#########################################################################################
################################ ADAPT CASE: CONSTRAINTS ################################
#########################################################################################

def check_type_ingredient_selected_constraints(originalCase, neighbours, typeIngredientsSelected, col,
                                               allSelected=None, allForbidden=None):
    """
    Adapt the recipe to fulfill the selected preferences of the user according to a specific type or attribute.

    :param newCase: current recipe of the cocktail.
    :param neighbours: retrieved similar cases to the user preferences.
    :param typeIngredientsSelected: user selected attributes.
    :param col: attribute or column to look for the type of ingredients.
    :param allSelected: all selected user preferences (ingredients, types of alcohol and tastes).
    :param allForbidden: all forbidden user preferences (ingredients, types of alcohol and tastes).
    :return: recipe so that the selected user preferences of a specific attribute are in the cocktail.
    """

    newCase = originalCase.copy()

    # Constraint already in the case or not
    typeIngredientsNewCase = originalCase[col]
    for typeIngredient in typeIngredientsSelected:

        # Constraint not in the current solution, use the other retrieve cases
        if typeIngredient not in typeIngredientsNewCase:

            if col == 'ingredients':
                newCase = incorporate_new_ingredient(newCase, typeIngredient, neighbours,
                                                     typeIngredientsSelected.copy(), allSelected, allForbidden)
            else:
                newCase = incorporate_new_type_ingredient(newCase, typeIngredient, neighbours, col,
                                                          allSelected, allForbidden)

    return newCase

def check_type_ingredient_forbidden_constraints(originalCase, typeIngredientsForbidden, col):
    """
    Adapt the recipe to fulfill the forbidden preferences of the user according to a specific type or attribute.

    :param originalCase: current recipe of the cocktail.
    :param typeIngredientsForbidden: user forbidden attributes.
    :param col: attribute or column to look for the type of ingredients.
    :return: recipe so that all the forbidden user preferences of a specific attribute are not present in the cocktail.
    """

    newCase = originalCase.copy()

    # Constraint already in the case or not
    typeIngredientsNewCase = originalCase[col]
    for typeIngredient in typeIngredientsForbidden:

        # Constraint in the current solution, remove it
        if typeIngredient in typeIngredientsNewCase:
            newCase = remove_ingredients_from_case(newCase, typeIngredient, col)

    return newCase

def adapt_with_selected_constraints(originalCase, neighbours, query, excludedQuery):
    """
    Adapt the recipe to fulfill all the selected preferences of the user.

    :param originalCase: current recipe of the cocktail.
    :param neighbours: retrieved similar cases to the user preferences.
    :param query: selected user preferences.
    :param excludedQuery: forbidden user preferences.
    :return: recipe so that all the selected user preferences are in the cocktail.
    """

    newCase = originalCase.copy()

    # Query selected constraints
    ingredientsSelected = query['ingredients']; ingredientsSelected.discard('nan')
    alcTypesSelected = query['alcTypes']; alcTypesSelected.discard('nan')
    categoriesSelected = query['categories']; categoriesSelected.discard('nan')
    tastesSelected = query['tastes']; tastesSelected.discard('nan')
    allSelected = [ingredientsSelected, alcTypesSelected, tastesSelected]

    # Query forbidden constraints
    ingredientsForbidden = excludedQuery['ingredients_excluded']; ingredientsForbidden.discard('nan')
    alcTypesForbidden = excludedQuery['alcTypes_excluded']; alcTypesForbidden.discard('nan')
    # categoriesSelected = excludedQuery['categories_excluded']; categoriesSelected.discard('nan')
    tastesForbidden = excludedQuery['tastes_excluded']; tastesForbidden.discard('nan')
    allForbidden = [ingredientsForbidden, alcTypesForbidden, tastesForbidden]

    # Ingredients, alcohol type, categories and tastes constraints
    newCase = check_type_ingredient_selected_constraints(newCase, neighbours, ingredientsSelected, 'ingredients',
                                                         allSelected, allForbidden)
    newCase = check_type_ingredient_selected_constraints(newCase, neighbours, alcTypesSelected, 'alcTypes',
                                                         allSelected, allForbidden)
    newCase = check_type_ingredient_selected_constraints(newCase, neighbours, tastesSelected, 'tastes',
                                                         allSelected, allForbidden)

    return newCase


def adapt_with_forbidden_constraints(originalCase, excludedQuery):
    """
    Adapt the recipe to fulfill all the forbidden preferences of the user.

    :param originalCase: current recipe of the cocktail.
    :param excludedQuery: forbidden user preferences.
    :return: recipe so that all the forbidden user preferences are not present in the cocktail.
    """

    newCase = originalCase.copy()

    # Query forbidden constraints
    ingredientsForbidden = excludedQuery['ingredients_excluded']; ingredientsForbidden.discard('nan')
    alcTypesForbidden = excludedQuery['alcTypes_excluded']; alcTypesForbidden.discard('nan')
    # categoriesSelected = excludedQuery['categories']; categoriesSelected.discard('nan')
    tastesForbidden = excludedQuery['tastes_excluded']; tastesForbidden.discard('nan')

    # Ingredients, alcohol type, categories and tastes constraints
    newCase = check_type_ingredient_forbidden_constraints(newCase, ingredientsForbidden, 'ingredients')
    newCase = check_type_ingredient_forbidden_constraints(newCase, alcTypesForbidden, 'alcTypes')
    newCase = check_type_ingredient_forbidden_constraints(newCase, tastesForbidden, 'tastes')

    return newCase


def reuse(neighbours, query, excludedQuery):
    """
    Reuse phase of the CBR system. It consists on adapting the retrieved cases to obtain a solution according to the
    specifications or preferences of the user.

    :param neighbours: retrieved similar cases to the user preferences.
    :param query: selected user preferences.
    :param excludedQuery: forbidden user preferences.
    :return: adapted recipe according to the user preferences considered as a solution.
    """

    # Base new case (first neighbour)
    originalCase = neighbours.iloc[0]
    neighbours = neighbours[1:]
    totalMeasures = np.sum(originalCase['measures'])

    newCase = originalCase.copy()

    # Adapt using constraints of the user
    newCase = adapt_with_selected_constraints(newCase, neighbours, query, excludedQuery)
    newCase = adapt_with_forbidden_constraints(newCase, excludedQuery)

    # Post-processing
    newCase = normalize_measures(newCase, totalMeasures)

    return newCase

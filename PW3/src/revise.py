import pandas as pd
import numpy as np
from src.utils import getJaccardSimilarity, percentageQuantity

path_to_root_folder = "./"

PONDERATION = np.array([0.50, 0.20, 0.20, 0.10])

DELETED_DISTANCE_THRESHOLD = 0.1

def updateUtility(cocktails, indexes):
    """
    Update the utility of the cocktails in the indexes
    
    :param cocktails: the cocktails dataframe
    :param indexes: the indexes of the cocktails to update
    """

    cases = cocktails.loc[indexes]
    S = cases.retrievedSuccess
    UaS = cases.usedSuccess
    F = cases.retrievedFailure
    UaF = cases.usedFailure

    total = S + F
    success = np.where(total != 0, np.divide(UaS, total), 0)
    failures = np.where(total != 0, np.divide(UaF, total), 0)

    cocktails.loc[indexes, "utility"] = (success-failures+1.0)/2.0

def getDistanceClosestDeleted(adaptedCase, cocktailsDeleted):
    """
    Get the distance between the adapted case and the closest deleted cocktail

    :param adaptedCase: the adapted case
    :param cocktailsDeleted: the deleted cocktails
    :return: the distance between the adapted case and the closest deleted cocktail
    """

    # Compute the similarities
    columnsForDistance = ["ingredients", "alcTypes", "tastes"]
    jaccardSimilarities = np.zeros((len(cocktailsDeleted), len(columnsForDistance)+1))
    for i, col in enumerate(columnsForDistance):
        caseSet = set(adaptedCase[col])
        jaccardSimilarities[:, i] = cocktailsDeleted[col].apply(lambda row: getJaccardSimilarity(set(row), caseSet))

    jaccardSimilarities[:, -1] = cocktailsDeleted["categories"].apply(lambda row: int(adaptedCase["categories"] == row))

    # Take into account quantities  
    measureCorrect = cocktailsDeleted[["ingredients", "measuresNorm"]]\
        .apply(lambda row: percentageQuantity(row, adaptedCase['ingredients']), axis=1)

    # Multiply the distances by the percentage of correct quantity and utility
    jaccardDistances = (1- jaccardSimilarities) * PONDERATION
    distances = np.sum(jaccardDistances, axis=1) * measureCorrect

    return np.sort(distances)[0]

def revise(cocktails, cocktailsDeleted, adaptedCase, excludedQuery, retrievedCases, evaluation, verbose=True): 
    """
    Revise the adapted case and determine whether it is valid or not. 
    If it is valid, contninue to the next step, otherwise, rollback.

    :param cocktails: the cocktails dataframe
    :param cocktailsDeleted: the deleted cocktails dataframe
    :param adaptedCase: the adapted case
    :param excludedQuery: the excluded query
    :param retrievedCases: the retrieved cases
    :param evaluation: the evaluation of the adapted case (user feedback)
    :param verbose: whether to print or not
    :return: the adapted case if it is valid, None otherwise
    """

    # Similar to deleted
    similarToDeleted = False
    if len(cocktailsDeleted) > 0:
        distanceClosestDeleted = getDistanceClosestDeleted(adaptedCase, cocktailsDeleted)
        similarToDeleted = distanceClosestDeleted < DELETED_DISTANCE_THRESHOLD
        if similarToDeleted:
            evaluation = 0

    # Update the utility of the retrieved cases
    middlePoint = 6/10
    evaluation = evaluation/10 # [0,1]
    success = 0
    failure = 0
    if evaluation <= middlePoint:
        failure = 1-evaluation/middlePoint
    else:
        success = 1-(1-evaluation)/(1-middlePoint)

    cocktails.loc[retrievedCases.index, "retrievedSuccess"] += success
    cocktails.loc[retrievedCases.index[0], "usedSuccess"] += success
    cocktails.loc[retrievedCases.index, "retrievedFailure"] += failure
    cocktails.loc[retrievedCases.index[0], "usedFailure"] += failure
    updateUtility(cocktails, retrievedCases.index)
    
    cocktails.to_csv(path_to_root_folder+'data/cocktailsGrouped.csv', index=False)

    if similarToDeleted:
        if verbose: print("Too similar to a deleted cocktail")
        return None, "case was too similar to a deleted cocktail"
    
    # Bad evaluation, rollback
    if evaluation < 0.5:
        if verbose: print("Insufficient evaluation")
        cocktailsDeleted.loc[len(cocktailsDeleted)] = adaptedCase.values
        cocktailsDeleted.to_csv(path_to_root_folder+"data/cocktailsGroupedDeleted.csv", index=False)
        return None, "evaluation was not >= 5"
    
    ## Revise constraints in the adapted case
    for col in excludedQuery:
        colCase = col.replace("_excluded","")
        inter = set(adaptedCase[colCase]).intersection(excludedQuery[col])
        if inter:
            if verbose: print(f"{list(inter)} constraints at column \"{colCase}\" are not satisfied")
            return None, "some constraint were not fulfilled"

    evaluatedCase = adaptedCase.copy()
    evaluatedCase.loc["utility"] = evaluation
    return evaluatedCase, ""

import ast
import pandas as pd
import numpy as np
from src.utils import getJaccardSimilarity, percentageQuantity

PONDERATION = np.array([0.50, 0.25, 0.25])

# Retain threshold
RETAIN_THRESHOLD = 0.1

# Forget threshold
UTILITY_THRESHOLD = 0.2
RETRIEVED_FAILURE_THRESHOLD = 1
path_to_root_folder = './'

def distance_closest_case(revised_case, closest_case):
    """
    Compute the distance between the revised case and the closest case from the original query

    :param revised_case: the revised case
    :param closest_case: the closest case from the original query
    :return: the distance between the two cases
    """

    # Compute the similarities
    columnsForDistance = ["ingredients", "alcTypes", "tastes"]
    jaccardSimilarities = [0, 0, 0]

    for i, col in enumerate(columnsForDistance):
        querySet = set(revised_case[col])
        closestSet = set(closest_case[col])
        jaccardSimilarities[i] = getJaccardSimilarity(querySet, closestSet)

    # Take into account quantities
    measureCorrect = percentageQuantity(revised_case, closest_case['ingredients'], literal=True)

    # Multiply the distances by the percentage of correct quantity and utility
    jaccardSimilarities = np.array(jaccardSimilarities)
    jaccardDistances = (1- jaccardSimilarities) * PONDERATION
    distance = np.sum(jaccardDistances) * measureCorrect

    return distance

def forget(dataset):
    """
    Forget the cases that are not useful anymore

    :param dataset: the database
    :return: the rows to delete
    """

    # Get the rows to delete based on their utility and retrievedFailure
    rows_to_delete = dataset[(dataset['utility'] < UTILITY_THRESHOLD) & (dataset['retrievedFailure'] > RETRIEVED_FAILURE_THRESHOLD)]

    if len(rows_to_delete) > 0:
        dataset = dataset.drop(rows_to_delete.index)
        # Save the database
        dataset.to_csv(path_to_root_folder+"data/cocktailsGrouped.csv", index=False)
        
    return rows_to_delete


def retain(revised_case, closest_case, dataset, dataset_deleted):
    """
    Retain the revised case if it is different enough from the closest case from the original query.
    Forget the cases that are not useful anymore.

    :param revised_case: the revised case
    :param closest_case: the closest case from the original query
    :param dataset: the database
    :param dataset_deleted: the database of deleted cases
    :return: wheter to the new instance has been added, the distance between the two cases, the number of cases deleted
    """

    # Get the distances between the revised case and the closest case
    distance = distance_closest_case(revised_case, closest_case)

    # If the distance is greater than the threshold, retain the case
    saved = False
    if distance > RETAIN_THRESHOLD:
        # Add the case to the database
        dataset.loc[len(dataset)] = revised_case
        dataset.to_csv(path_to_root_folder+"data/cocktailsGrouped.csv", index=False)
        saved = True

    # Get which cases should be forgotten
    rows_to_delete = forget(dataset)

    # Delete the cases from the database
    if len(rows_to_delete) > 0:
        # Add the cases to the deleted database
        for value in rows_to_delete.values:
            dataset_deleted.loc[len(dataset_deleted)] = value

        dataset_deleted.to_csv(path_to_root_folder+"data/cocktailsGroupedDeleted.csv", index=False)

    return saved, distance, len(dataset_deleted)

import pandas as pd
from typing import Set
import numpy as np
from src.utils import getJaccardSimilarity, percentageQuantity


PONDERATION = np.array([0.50, 0.20, 0.20, 0.10])

def retrieve(query, dataset, excludedQuery=None, n=5):
    """
    Retrieve phase of the CBR system. It consists of obtaining the most similar cases according to the user preferences.

    :param query: selected user preferences.
    :param dataset: Case Library with the cases.
    :param excludedQuery: forbidden user preferences.
    :param n: number of similar cases to retrieve.
    :return: n most similar cases according to the user preferences.
    """

    # Check that thr query attributes are the correct ones
    columnsQuery = ["ingredients", "alcTypes", "tastes", "categories"]
    assert np.all([col in query for col in columnsQuery]), \
        f"The query must be a dict with the columns {columnsQuery}"
    
    columnsQuery = [col+"_excluded" for col in columnsQuery]
    if excludedQuery:
        assert np.all([col in excludedQuery for col in columnsQuery]), \
            f"The excluded query must be a dict with the columns {columnsQuery}"
    else:
        excludedQuery = {col: [] for col in columnsQuery}

    # Filter by category
    categories = excludedQuery['categories_excluded']
    filteredDataset = dataset[~dataset['categories'].isin(categories)]

    # Compute the similarities
    columnsForDistance = ["ingredients", "alcTypes", "tastes"]
    jaccardSimilarities = np.zeros((len(filteredDataset), len(columnsForDistance)+1))
    jaccardSimilaritiesExcluded = np.zeros((len(filteredDataset), len(columnsForDistance)+1))
    for i, col in enumerate(columnsForDistance):
        querySet = set(query[col])
        jaccardSimilarities[:, i] = filteredDataset[col].apply(lambda row: getJaccardSimilarity(set(row), querySet))
        queryExcludedSet = set(excludedQuery[col+"_excluded"])
        jaccardSimilaritiesExcluded[:, i] = filteredDataset[col].apply(lambda row: getJaccardSimilarity(set(row), queryExcludedSet))

    jaccardSimilarities[:, -1] = filteredDataset["categories"].apply(lambda row: int(np.any(
        [r == row for r in query["categories"]])))

    # Take into account quantities  
    measureCorrect = filteredDataset[["ingredients", "measuresNorm"]]\
        .apply(lambda row: percentageQuantity(row, query['ingredients']), axis=1)

    # Multiply the distances by the percentage of correct quantity and utility
    jaccardSimilarity = np.zeros_like(jaccardSimilarities)
    jaccardSimilarity[:-1] = (jaccardSimilarities[:-1]-jaccardSimilaritiesExcluded[:-1]+1)/2
    jaccardDistances = (1- jaccardSimilarity) * PONDERATION
    distances = np.sum(jaccardDistances, axis=1) * measureCorrect
    distances *= 1-filteredDataset["utility"]

    # Compute the neighbours based on the distances
    nNeighbours = np.argsort(distances)[:n]
    indexes = filteredDataset.iloc[nNeighbours,:].index

    return filteredDataset.loc[indexes]

import ast
from typing import Set

def getJaccardSimilarity(set1: Set, set2: Set):
    """
    Compute Jaccard similarity between 2 sets of elements.

    :param set1: first set of elements.
    :param set2: second set of elements.
    :return: Jaccard similarity between the 2 input sets.
    """

    set1.discard("nan")
    set2.discard("nan")
    intersection = len(set1 & set2)
    union = len(set1 | set2)
    return intersection / union if union else 0

def percentageQuantity(row, queryIngredients, literal=False):
    """
    Compute the remaining quantity of the cocktail without the selected ingredients by the user.

    :param row: case or cocktail sample.
    :param queryIngredients: set of ingredients selected by the user.
    :return: proportion of the case corresponding to not selected ingredients by the user.
    """

    ingredients = row.ingredients
    measuresNorm = ast.literal_eval(row.measuresNorm) if literal else row.measuresNorm
    totalPercentage = 0
    for ing in set(queryIngredients) & set(ingredients):
        totalPercentage += float(measuresNorm[ingredients.index(ing)])
    return 1-totalPercentage

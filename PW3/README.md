# A CBR prototype for a synthetic task
## Cocktail preparation
Supervised and Experiential Learning (SEL) - Master in Artificial Intelligence

## Authors
- Sonia Rabanaque
- Armando Rodriguez
- Pablo Ruiz
- Hasnain Shafqat

## Running interface for the first time
These sections show how to create virtual environment for
our script and how to install dependencies. The first thing to do is to install a version of Python >= 7.0. We recommend Python 3.9.
1. Open folder in terminal
```bash
cd <root_folder_of_project>/
```
2. Create virtual env
```bash
python3 -m venv venv/
```
3. Open virtual env
```bash
source venv/bin/activate
```
4. Install required dependencies
```bash
pip install -r requirements.txt
```

## Execute code
- Or open the already hosted public application:

    https://cocktails-cbr.streamlit.app/
1. Open folder in terminal
```bash
cd <root_folder_of_project>/
```
2. Running the interface code. The localhost will be automatically opened.
 ```bash
 streamlit run interface.py
 ```
3. Close virtual environment
```bash
deactivate
```

## Directory structure:

- .streamlit/: contains the configuration of the interface
- data/: contains the data used for the experiments
    - cocktails.csv: contains the dataset of the cocktails
    - cocktailsGrouped.csv: contains the dataset of the cocktails grouped (1 cocktail per row)
    - cocktailsGroupedDeleted.csv: contains the deleted cases of the dataset
    - groupCSV.py: contains the code to define our Case Library
    - informationIngridients.py: contains the code to obtais information of the ingredients
    - ingredientsInformation.csv: contains the information of the ingredients and quantities
- docs/: contains the documentation of the project
    - PW3-Paper.pdf: contains the documentation of the project
    - PW3-UserManual.pdf: contains the user manual of the project
    - PW3-Slides.pdf: contains the slides of the project
- images/: contains the images of the project
    - cocktails.jpg: image of cocktails for the Stramlit app
- results/: contains the results of the mass testing
    - massTesting100.txt: contains the results of the mass testing with 100 queries
    - massTesting200.txt: contains the results of the mass testing with 200 queries
    - massTesting300.txt: contains the results of the mass testing with 300 queries
    - massTesting400.txt: contains the results of the mass testing with 400 queries
    - massTesting500.txt: contains the results of the mass testing with 500 queries
    - massTesting600.txt: contains the results of the mass testing with 600 queries
    - massTesting700.txt: contains the results of the mass testing with 700 queries
    - massTesting800.txt: contains the results of the mass testing with 800 queries
    - massTesting900.txt: contains the results of the mass testing with 900 queries
    - massTesting1000.txt: contains the results of the mass testing with 1000 queries
- src/: contains the source code of the project
    - analysis.py: contains the code to perform the analysis of the dataset
    - retain.py: contains the code to perform the RETAIN phase of the CBR
    - retrieve.py: contains the code to perform the RETRIEVE phase of the CBR
    - reuse.py: contains the code to perform the REUSE phase of the CBR
    - revise.py: contains the code to perform the REVISE phase of the CBR
    - utils.py: contains the code of additional functions used in different phases of the CBR
- interface.py: contains the code of the interface of the CBR in Streamlit
- massTesting.py: contains the code to perform the mass testing
- massTesting.sh: contains the script to execute the mass testing
- requirements.txt: contains the libraries needed to run the project

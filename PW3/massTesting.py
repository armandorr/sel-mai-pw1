import sys
from time import time
import numpy as np
import pandas as pd
from src.retrieve import retrieve
from src.reuse import reuse
from src.revise import revise
from src.retain import retain, forget

def read_csv_with_lists(file_path, columns=[]):
    converters = {column: eval for column in columns}
    return pd.read_csv(file_path, converters=converters)

# Read the Case Library
cocktails = read_csv_with_lists('./data/cocktailsGrouped.csv', 
                                ["ingredients", "alcTypes", "tastes", "measures", "measuresNorm",
                                 "measuresUnit", "measuresText", "valuesML", "valuesGR", "instructions"])

# Get all the possible values for each column
allIngredients = list(set(np.unique(np.concatenate(cocktails.ingredients.values))))
allCategories = list(set(np.unique(cocktails.categories.values)))
allTastes = list(set(np.unique(np.concatenate(cocktails.tastes.values))))
allTastes.remove("nan")
allTypes = list(set(np.unique(np.concatenate(cocktails.alcTypes.values))))
allTypes.remove("nan")

def generateQueries(nQueries, columns, maxOptions):
    """
    Generate random queries for mass testing

    :param nQueries: number of queries to generate
    :param columns: attributes to generate the queries
    :param maxOptions: maximum number of options to choose from
    :return: a list of queries
    """

    ingNums = np.random.randint(0, maxOptions+1, size=nQueries)
    catNums = np.random.randint(0, maxOptions+1, size=nQueries)
    tasNums = np.random.randint(0, maxOptions+1, size=nQueries)
    typNums = np.random.randint(0, maxOptions+1, size=nQueries)

    queries = []

    for i,c,a,y in zip(ingNums,catNums,tasNums,typNums):
        ing = set(np.random.choice(allIngredients, size=i, replace=False))
        cat = set(np.random.choice(allCategories, size=c, replace=False))
        tas = set(np.random.choice(allTastes, size=a, replace=False))
        typ = set(np.random.choice(allTypes, size=y, replace=False))
        queries.append({c:a for c, a in zip(columns,[ing, typ, tas, cat])})

    return queries


def massTesting(tests = 100):
    """
    Run a mass testing of the system

    :param tests: number of tests to run
    """

    # Generate random queries
    np.random.seed(42)
    evaluations = np.random.randint(0, 6, size=tests)+5
    queryColumns = ["ingredients", "alcTypes", "tastes", "categories"]
    queries = generateQueries(tests, queryColumns, 5)

    queryExcludedColumns = [col+"_excluded" for col in queryColumns]
    queriesExcluded = generateQueries(tests, queryExcludedColumns, 5)

    saveds = 0
    rets = []
    adas = []
    evas = []
    rats = []

    # Run the tests pipeline
    ini0 = time()
    for num, (query, queryExcluded, evaluation) in enumerate(zip(queries, queriesExcluded, evaluations)):
        finished = False
        retAux = 0
        adaAux = 0
        evaAux = 0
        counter = 0
        while not finished:
            if counter == 5:
                query = generateQueries(1, queryColumns, 5)[0]
                queryExcluded = generateQueries(1, queryExcludedColumns, 5)[0]
                counter = 0
            
            ini = time()
            cocktails = read_csv_with_lists('./data/cocktailsGrouped.csv', 
                                    ["ingredients", "alcTypes", "tastes", "measures", "measuresNorm",
                                    "measuresUnit", "measuresText", "valuesML", "valuesGR", "instructions"])
            cocktailsDeleted = read_csv_with_lists('./data/cocktailsGroupedDeleted.csv', 
                                ["ingredients", "alcTypes", "tastes", "measures", "measuresNorm",
                                 "measuresUnit", "measuresText", "valuesML", "valuesGR", "valuesGarnish", "typesGarnish", "instructions"])
            retrievedCases = retrieve(query, cocktails, queryExcluded, n=10)
            ret = time()
            retAux += ret-ini
            adaptedCase = reuse(retrievedCases, query, queryExcluded)
            ada = time()
            adaAux += ada-ret
            evaluatedCase, _ = revise(cocktails, cocktailsDeleted, adaptedCase, queryExcluded, retrievedCases, evaluation, verbose=False)
            eva = time()
            evaAux += eva-ada

            finished = evaluatedCase is not None 
            evaluation = np.random.randint(0, 11)
            counter += 1
        
        saved, _, _ = retain(evaluatedCase, retrievedCases.iloc[0], cocktails, cocktailsDeleted)
        rat = time()
        
        saveds += int(saved)
        rets.append(retAux)
        adas.append(adaAux)
        evas.append(evaAux)
        rats.append(rat-eva)
    end = time()

    # Print results
    print("Total time =", end-ini0)
    print("Total time normalized =", (end-ini0)/tests)

    cocktails_deleted = read_csv_with_lists('./data/cocktailsGroupedDeleted.csv')
    print("Saved =", saveds, "out of", tests)
    print("Deleted cases =", len(cocktails_deleted))

    print("Average time:")
    print(" Retrive =", np.average(rets))
    print(" Reuse =", np.average(adas))
    print(" Revise =", np.average(evas))
    print(" Retain =", np.average(rats))

    print("Arrays:")
    print(" Retrive =", rets)
    print(" Reuse =", adas)
    print(" Revise =", evas)
    print(" Retain =", rats)


massTesting(int(sys.argv[1]))
